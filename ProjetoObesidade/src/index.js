import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import DescritorUnidade1 from "./components/DescritorUnidade1.js";
import DescritorUnidade2 from "./components/DescritorUnidade2.js";
import DescritorUnidade3 from "./components/DescritorUnidade3.js";
import DescritorCreditos from "./components/DescritorCreditos.js";



import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.min.css";

import { BrowserRouter, Switch, Route, HashRouter } from "react-router-dom";

ReactDOM.render(
	<HashRouter>
		<Switch>
			<Route path="/" exact={true} component={App} />
			<Route path="/un1" component={DescritorUnidade1} />
			<Route path="/un2" component={DescritorUnidade2} />
			<Route path="/un3" component={DescritorUnidade3} />
			<Route path="/creditos" component={DescritorCreditos} />

		</Switch>
	</HashRouter>,
	document.getElementById("root")
);


serviceWorker.unregister();
