import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from "reactstrap";

import seta from "../img/seta.png";

import "./css/Modal.css";
import "./css/Slider.css";

const items = [
	{
		texto: (
			<div className="fundoSliderTexto">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"><b>COM BAIXO PESO</b></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTexto">
				<div className="container-text">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"><b>COM PESO ADEQUADO</b></p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"></p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
								</p>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTexto">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>COM PESO ELEVADO OU GANHO EXCESSIVO</b>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	}
];

class Slider extends React.Component {
	constructor() {
		super();
		this.state = { activeIndex: 0, isHiddenCaixa: true };
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}

	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === items.length - 1
				? 0
				: this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === 0
				? items.length - 1
				: this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}

	render() {
		const { activeIndex } = this.state;

		const slides = items.map(item => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={item.texto}
				>
					<Container>
						<Row>
							<Col md="12" lg="12">
								{item.texto}
							</Col>
						</Row>
					</Container>
				</CarouselItem>
			);
		});

		return (
			<div className="slider1">
				<Container>
					<Row>
						<Col
							md="12"
							lg="12"
							className="espacamentoBottom espacamento"
						>
							<Carousel
								activeIndex={activeIndex}
								next={this.next}
								previous={this.previous}
								pause={false}
								ride="carousel"
								interval={false}
								slide={true}
							>
								<CarouselIndicators
									items={items}
									activeIndex={activeIndex}
									onClickHandler={this.goToIndex}
									className="espacamento zIndex"
								/>

								{slides}
								<br />
								<br />
								<br />
							</Carousel>
							<CarouselControl
								direction="prev"
								directionText="Previous"
								onClickHandler={this.previous}
								className=""
							>
								<img
									src={seta}
									className="responsive w100 setaIndex"
								/>
							</CarouselControl>
							{/*TODO: colocar seta aqui*/}
							<CarouselControl
								direction="next"
								directionText="Next"
								onClickHandler={this.next}
								className=""
							/>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Slider;
