import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";
import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import cliqueAqui from "../img/clique.png";
import iconeLink from "../img/link.png";

import iconeVideo from "../img/video.png";
import ScrollAnimation from "react-animate-on-scroll";
import "./css/Utilitarios.css";
import ModalPopUp from "./ModalExemplo.js";
import imgExemplo from "../img/un2/exemplo.png";


import {
  TituloSecaoBarra2,
  TituloSubSecaoBarra,
  CaixaTemplate,
  CaixaGeralRetorno,
  TituloSecaoBarraNova
} from "./Utilitarios.js";

class DivHideFinal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHidden: true
    };
  }

  toggleHidden() {
    this.setState({
      isHidden: !this.state.isHidden
    });
  }
  render(props) {
    return (
      <div>
        <Container
          onClick={this.toggleHidden.bind(this)}
          className="clickHide borda10 cursorClicavel"
        >
          <Row>              
              
            <Col md="12" lg="12" className="espacamento">
              <ModalPopUp
                      conceito={
                        <p>
                          Clique aqui para conhecer um
                          exemplo
                        </p>
                      }
                      textoConceito={
                        "A promoção de saúde é o “processo de capacitação da comunidade para atuar na melhoria da sua qualidade de vida e saúde, incluindo maior participação no controle desse processo” (CARTA DE OTTAWA, 1986)."
                      }
                      imgPopUp={
                        <img
                          src={imgExemplo}
                          className="responsive w100 borda10 "
                        />
                      }
                    ></ModalPopUp>
            </Col>
            <Col md="2" lg="2" xs="2" sm="2">
              <img src={cliqueAqui} className="responsive w40 imgTop" />
            </Col>
          </Row>
        </Container>

        {!this.state.isHidden && (
          <Child1
            textoCaixa={this.props.textoCaixa}
            fundoCaixa={this.props.fundoCaixa}
          />
        )}
      </div>
    );
  }
}

const Child1 = props => {
  return (
    <div className="espacamentoBottom">
      <Container className={"bordaCaixa espacamento " + props.fundoCaixa }>{props.textoCaixa}</Container>
    </div>
  );
};

export default DivHideFinal;
