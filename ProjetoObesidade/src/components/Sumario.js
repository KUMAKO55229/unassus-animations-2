import React from 'react';
import './css/Sumario.css';
import 'font-awesome/css/font-awesome.min.css';
import { Container, Row, Col } from 'reactstrap';
import './css/Utilitarios.css';



class Sumario extends React.Component {

	render() {	


		const titulosPt1 = this.props.titulosPt1;
		const listItems = titulosPt1.map((itens) =>
		<li className="tituloUnidade">{itens}</li>
		);

		const titulosPt2 = this.props.titulosPt2;
		const listItems2 = titulosPt2.map((itens) =>
		<li className="tituloUnidade">{itens}</li>
		);


		return (
			
			
			<div className={this.props.tipoSumario+ ' espacamento logoCentralSumario'} >
					<Container className=""> 
						<Row >
							<Col md="12" lg="12">							
								<p className="tituloSumario">Conteúdo</p>
							</Col>							
							<Col md="12" lg="12">							
								<ul className="centerBullets">{listItems}</ul>												
								<ul className="centerBullets">{listItems2}</ul>												
							</Col>	
							
							<Col md="12" lg="12">							
							<br/>
							</Col>							


						</Row>

					</Container>



					


				</div>			
			);
	}
};

export default Sumario;

