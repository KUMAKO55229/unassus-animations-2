import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from "reactstrap";

import seta from "../img/seta.png";

import "./css/Modal.css";
import "./css/Slider.css";

const items = [
	{
		texto: (
			<div className="fundoSliderTextoUn1">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p><b>Retenção de peso pós-parto</b></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p>O número de mulheres que iniciam a gestação com
							excesso de peso ou que ganham peso excessivo durante
							a gravidez é cada vez mais expressivo. Um estudo
							realizado em seis capitais brasileiras, com 5.564
							gestantes, encontrou uma prevalência de 19,2% de
							sobrepeso e de 5,5% de obesidade entre as gestantes.
							A retenção do peso ganho se mostrou um fator
							determinante da obesidade em mulheres, e seu
							desenvolvimento. Estudos importantes demonstraram o
							efeito do ganho de peso gestacional elevado assim
							como do estado nutricional pré-gestacional
							inadequado, no aumento da retenção de peso
							pós-parto, que, por sua vez, pode perpetuar um
							quadro de obesidade e de comorbidades associadas
							para o resto da vida da mulher. Estes dados reforçam
							a importância da atuação no monitoramento e na
							orientação à gestante quanto ao ganho de peso
							adequado e saudável ao longo da gravidez.</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTextoUn1">
				<div className="container-text">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									{" "}
									<b>Tromboembolismo venoso</b>
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"></p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									A obesidade em si e o período puerperal são
									fatores de risco para o tromboembolismo
									venoso. Se o nascimento ocorreu por
									cesariana, soma-se mais um fator de risco.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									As chances de ter um evento tromboembólico
									no pós-parto são maiores quanto maior for o
									grau de obesidade. Aumentando 2,5, 2,9 e 4,6
									vezes para mulheres com obesidade classe I,
									II e III respectivamente em comparação com
									as mulheres sem obesidade. O ganho de peso
									excessivo durante a gestação (mais de 22
									quilos) também aumenta o risco para eventos
									tromboembólicos em 1,5 vezes (LISONKOVA et
									al., 2017).
								</p>
							</Col>
						</Row>
					</Container>
				</div>

				{/*<Col md="4" lg="4">
					<img src={require("../img/frutas.jpeg")} className="responsive w100 imgEdited"/>
				</Col> */}
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTextoUn1">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>Infecção</b>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container id="sec14">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As mulheres com obesidade têm um risco aumentado
								de apresentarem quadros infecciosos no
								pós-parto. Independente da via de parto e apesar
								do uso de antibióticos profiláticos na
								cesariana.{" "}
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTextoUn1">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>Depressão Pós-parto</b>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container id="sec14">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As desordens psíquicas não são raras nessa fase
								da vida, para todas as mulheres. Para adultos em
								geral, a obesidade está associada com distúrbios
								mentais como depressão, transtornos alimentares
								e transtorno bipolar. Uma revisão sistemática de
								2014 encontrou uma possibilidade maior de
								distúrbios depressivos na gestação e no pós
								parto entre mulheres com obesidade (MOLYNEAUX et
								al., 2014).
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	}
];

class Slider extends React.Component {
	constructor() {
		super();
		this.state = { activeIndex: 0, isHiddenCaixa: true };
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}

	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === items.length - 1
				? 0
				: this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === 0
				? items.length - 1
				: this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}

	render() {
		const { activeIndex } = this.state;

		const slides = items.map(item => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={item.texto}
				>
					<Container>
						<Row>
							<Col md="12" lg="12">
								{item.texto}
							</Col>
						</Row>
					</Container>
				</CarouselItem>
			);
		});

		return (
			<div className="sliderUn1">
				<Container>
					<Row>
						<Col
							md="12"
							lg="12"
							className="espacamentoBottom espacamento"
						>
							<Carousel
								activeIndex={activeIndex}
								next={this.next}
								previous={this.previous}
								pause={false}
								ride="carousel"
								interval={false}
								slide={true}
							>
								<CarouselIndicators
									items={items}
									activeIndex={activeIndex}
									onClickHandler={this.goToIndex}
									className="espacamento zIndex"
								/>

								{slides}
								<br />
								<br />
								<br />
							</Carousel>
							<CarouselControl
								direction="prev"
								directionText="Previous"
								onClickHandler={this.previous}
								className=""
							>
								<img
									src={seta}
									className="responsive w100 setaIndex"
								/>
							</CarouselControl>
							{/*TODO: colocar seta aqui*/}
							<CarouselControl
								direction="next"
								directionText="Next"
								onClickHandler={this.next}
								className=""
							/>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Slider;
