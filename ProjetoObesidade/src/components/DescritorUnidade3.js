/* eslint-disable import/first */
import React from "react";
import Testeira from "./TesteiraCreditos.js";
import CapaUnidade from "./capa/CapaPP.js";
import Sumario from "./SumarioLink.js";
import Unidade from "./Unidade3.js";
import menuHamburguer from "../img/menuObesidade2.png";
import ReturnStickyButton from "./ReturnStickyButton.js";

import Botoes from "./botoes/Botoes.js";


import un1 from "../img/un3/un1.png";
import un2 from "../img/un3/un2.png";
import un3 from "../img/un3/un3.png";


const testeira = {
  nomeCursoPt1: "Promoção do Ganho de Peso",
  nomeCursoPt2: "Adequado na Gestação",
  nomeUnidadePt1: "AÇÕES COLETIVAS PARA O GANHO",
  nomeUnidadePt2: "DE PESO ADEQUADO NA GESTAÇÃO ",
  tipoBarra: "barraHamburguerLogo"
};

const sumario = {
  qntColunas: 2,
  tipoSumario: "sumarioUnidade"
};

const sumarioPt1 = [
  {
    id: "/un3#sec31",
    title:
      "ACOLHIMENTO, AÇÕES EDUCATIVAS E TROCA DE EXPERIÊNCIAS PARA GESTANTES"
  },
  {
    id: "/un3#sec32",
    title:
      "AÇÕES E PRÁTICAS COLETIVAS DE EDUCAÇÃO NUTRICIONAL PARA UMA ALIMENTAÇÃO ADEQUADA E SAUDÁVEL NA GESTAÇÃO"
  }
];

const sumarioPt2 = [
  {
    id: "/un3#sec33",
    title: "ESTRATÉGIAS PARA LIDAR COM SINTOMAS COMUNS NA GESTAÇÃO"
  },
  {
    id: "/un3#sec34",
    title: "PLANEJAMENTO EM FAMÍLIA PARA PREVENÇÃO DE SOBREPESO E OBESIDADE"
  }
];

const sumarioTitulosPt1 = [
  "ACOLHIMENTO, AÇÕES EDUCATIVAS E TROCA DE EXPERIÊNCIAS PARA GESTANTES",
  "AÇÕES E PRÁTICAS COLETIVAS DE EDUCAÇÃO NUTRICIONAL PARA UMA ALIMENTAÇÃO ADEQUADA E SAUDÁVEL NA GESTAÇÃO"
];

const sumarioTitulosPt2 = [
  "ESTRATÉGIAS PARA LIDAR COM SINTOMAS COMUNS NA GESTAÇÃO",
  "PLANEJAMENTO EM FAMÍLIA PARA PREVENÇÃO DE SOBREPESO E OBESIDADE"
];

const linkCurso = "https://unasus-cp.moodle.ufsc.br/course/view.php?id=472";

const unidade = {
  tipoUnidade: "barraUnidade",
  corBarra: "blue"
};

function DescritorUnidade3() {
  return (
    <div className="DescritorUnidade3">
      {/*<Testeira nomeCursoPt1={testeira.nomeCursoPt1} nomeCursoPt2 ={testeira.nomeCursoPt2} 
        nomeUnidadePt1={testeira.nomeUnidadePt1} nomeUnidadePt2={testeira.nomeUnidadePt2} 
        tipoBarra={testeira.tipoBarra} menuHamburguer={menuHamburguer}/> */}
      <div id="top">
        <CapaUnidade
          tituloPt1={testeira.nomeUnidadePt1}
          tituloPt2={testeira.nomeUnidadePt2}
        />
        <Testeira
          nomeCursoPt1={testeira.nomeCursoPt1}
          nomeCursoPt2={testeira.nomeCursoPt2}
          nomeUnidadePt1={testeira.nomeUnidadePt1}
          nomeUnidadePt2={testeira.nomeUnidadePt2}
          tipoBarra={testeira.tipoBarra}
          menuHamburguer={menuHamburguer}
        />
        <Sumario
          tipoSumario={sumario.tipoSumario}
          titulosPt1={sumarioPt1}
          titulosPt2={sumarioPt2}
        />
      </div>
      <ReturnStickyButton linkCurso={linkCurso} />

      <Unidade tipoUnidade={unidade.tipoUnidade} corBarra={unidade.corBarra} />
      <Botoes
        un1Path={"/un1#top"}
        un2Path={"/un2#top"}
        un3Path={"/un3#top"}
        un1={un1}
        un2={un2}
        un3={un3}
      />
    </div>
  );
}

export default DescritorUnidade3;
