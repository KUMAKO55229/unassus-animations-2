import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "./css/Unidade2.css";
import "font-awesome/css/font-awesome.min.css";
import ScrollAnimation from "react-animate-on-scroll";
import imgUltra from "../img/un2/ultra.png";
import imgExemplo from "../img/un2/exemplo.png";
import logoCentral from "./capa/img/logoCentral.png";
import arco from "./capa/img/arco.png";
import { Animated } from "react-animated-css";
import classnames from "classnames";
// import AddClass from "./AddClass.js";
import { Dragact } from 'dragact';
//import { useFlipAnimation } from 'react-easy-flip';


//Tree Menu
import TreeMenu from 'react-simple-tree-menu';



import img13 from "../img/imgLegumes.png";
import clique from "../img/clique.png";
import {
Container,
Row,
Col,
Button,
Modal,
ModalHeader,
ModalBody,
ModalFooter,
CarouselItem,
CarouselControl,
CarouselIndicators,
CarouselCaption,

TabContent,
TabPane,
Nav,
NavItem,
NavLink,
Card,
CardTitle,
CardText
} from "reactstrap";
import { caixa } from "./Utilitarios.js";
import { criarLink } from "./Utilitarios.js";
import {
TituloSecaoBarra2,
TituloSubSecaoBarra,
CaixaTemplate,
CaixaGeralRetorno,
TituloSecaoBarraNova,
TituloSubSecaoBarraNova
} from "./Utilitarios.js";

import Carousel from 're-carousel';
import imgHeader1 from "../img/un2/icon1.png";
import imgHeader2 from "../img/un2/icon1.png";
import ModalPopUp from "./ModalPopUp.js";

import ReturnStickyButton from "./ReturnStickyButton.js";

import IconeHide from "./IconeNota.js";
import arrow from "../img/bullet.png";
import DivHide from "./DivClickHide.js";
// import changeImgPlace from "./changeImgPlace.js";

import img01 from "../img/un1/img01.jpeg";
import n1 from "../img/n1.png";
import n2 from "../img/n2.png";
import img02 from "../img/un1/img02.jpeg";
import img03 from "../img/un1/img03.png";

import infob from "../img/un1/infob.png";
import info02 from "../img/un1/info02.png";
import info02a from "../img/un1/info02a.png";

import quadro01a from "../img/un1/quadro01a.png";
import quadro01 from "../img/un1/quadro01.png";
import quadro5 from "../img/un1/quadro5.png";
import quadro6 from "../img/un1/quadro6.png";
import quadro02 from "../img/un1/quadro02.png";
import quadro3 from "../img/un1/quadro3.png";
import img04 from "../img/un2/img04.png";
import info04 from "../img/un1/info04.png";
import quadro4 from "../img/un1/quadro4.png";

import img05 from "../img/un2/img05.png";
import img06 from "../img/un2/img06.png";
import img07 from "../img/un2/img07.png";
import img07b from "../img/un2/img07b.png";

import imgCobrinhas from "../img/un1/Mooc3v1-59.png";

import img01a from "../img/un1/img01a.png";
import img08 from "../img/un2/img08.png";
import img09 from "../img/un2/img09.png";
import img10 from "../img/un2/img10.jpg";
import img11 from "../img/un2/img11.png";
import tabG from "../img/un1/Mooc3v1-51.png";
import tabG2 from "../img/un1/Mooc3v2-62.png";

import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import iconeLink from "../img/link.png";

import imgIMC1 from "../img/uni1/infoIMC1.png";
import imgIMC2 from "../img/uni1/infoIMC4.png";

import click from "../img/click.png";
import cliqueAqui from "../img/clique.png";

import mulheres from "../img/un1/Mooc3v1-60.png";


import Slider from "./Slider.js";
import SliderTab from "./Slider.js";
import Slider2 from "./SliderUn2_2.js";

import Accordion from "./Accord.js";
import imgFundo from "../img/testeiraInfografico.png";
const fakeData = [
    { GridX: 0, GridY: 0, w: 4, h: 2, key: '0' },
    { GridX: 0, GridY: 0, w: 4, h: 2, key: '1' },
    { GridX: 0, GridY: 0, w: 4, h: 2, key: '2' },
     { GridX: 0, GridY: 0, w: 4, h: 2, key: '3' },
      { GridX: 0, GridY: 0, w: 4, h: 2, key: '4' },
       { GridX: 0, GridY: 0, w: 4, h: 2, key: '5' },
        { GridX: 0, GridY: 0, w: 4, h: 2, key: '6' },
         { GridX: 0, GridY: 0, w: 4, h: 2, key: '7' },
          { GridX: 0, GridY: 0, w: 4, h: 2, key: '8' },
           { GridX: 0, GridY: 0, w: 4, h: 2, key: '9' },
         { GridX: 0, GridY: 0, w: 4, h: 2, key: '10' },
          { GridX: 0, GridY: 0, w: 4, h: 2, key: '11' }

]


const getblockStyle = (isDragging) => {
    return {
        background: isDragging ? '#1890ff' : 'white',
    }

}

class Unidade extends React.Component {

constructor() {
super();
this.state = {
isHiddenCaixa: true,
isHiddenCaixa2: true,
modal: false,
activeTab: "0"
};




this.toggle = this.toggle.bind(this);
}	

toggle() {
this.setState(prevState => ({
modal: !prevState.modal
}));
}
toggleCaixa() {
this.setState({
isHiddenCaixa: !this.state.isHiddenCaixa
});
}

toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}

toggleCaixa2() {
this.setState({
isHiddenCaixa2: !this.state.isHiddenCaixa2
});
}
componentDidMount() {
setTimeout(() => {
//var height01 = document.getElementById("textoImg01").clientHeight;
var height02 = document.getElementById("textoImgQuadro")
.clientHeight;

var heightN =
//document.getElementById("textoImg02").clientHeight +
//	document.getElementById("textoImg03").clientHeight +
16;

this.setState({
//height01,
height02,
heightN
});
}, 1000);}

	render() {
		return (
		<div>
			<div id="textoImgQuadro"></div>

			<TituloSecaoBarraNova
			tipoBarra={
			this.props.tipoUnidade + " espacamento degradeTitulo3"
		}
		titulo={"ANIMAÇÕES HTML"}
		/>

		<div className="espacamento" id="sec1"></div>

		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 1 - Fade-In"}
		/>
		<ScrollAnimation animateIn="fadeIn" duration="2">
			<img
			src={img01}
			className="w40 centerImg"
			/>
		</ScrollAnimation>

		<div className="espacamentoTop35"id="sec2"></div>

		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 2 - Fade-Out"}
		/>
		<ScrollAnimation  animateIn="fadeOut" duration="2">
			<img
			src={img01}
			className="w40 centerImg"
			/>
		</ScrollAnimation>

		<div id="sec3" className="espacamentoTop35"></div>
		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 3 - Sliders"}
		/>

		<Slider2 className="" />
		<SliderTab className="" />

		<div className="espacamentoTop35"></div>


		<div className="espacamento"></div>

		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 4 - Dropdown informação"}
		/>
		<div id="sec4"></div>
		<Container className="espacamentoTop35">
			<Row>
				<Col md="12" lg="12">
				<p className="textoUnidade">

					<IconeHide
					textoClique={"Clique no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaDestaque"}
					icone={iconeDestaque}
					textoUnidade={
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				}
				inserirLink={false}
				proporcaoCol1={1}
				proporcaoCol2={11}
				></IconeHide>{" "}
			</p>
		</Col>
		</Row>
		</Container>

		<div className="espacamentoTop35" id="sec5"></div>

		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 5 - Quadro dropdown"}
		/>

		<DivHide
		textoClique={[
		<b>Clique aqui </b>
		]}
		textoCaixa={
		<div>
			<Container>
				<Row>
					<Col md="12" lg="12">
					<p className="textoUnidade">
						<b>
							The standard Lorem Ipsum passage, used since the 1500s
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

							Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
							"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
						</b>
					</p>
				</Col>
			</Row>
		</Container>

		</div>
		}
		/>

		<div className="espacamentoTop35" id="sec6"></div>

		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 6 - Sequência de quadros dropdown"}
		/>

		<Container>
			<Row>
				<Col md="12" lg="12">
				<Accordion
				title={
				<p className="">
					<b>Quadro 1</b>
				</p>
			}
			content={
			<div className="fundoA11 borda10 espacamento">
				<Container>
					<Row>
						<Col md="12" lg="12">
						<p>
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

						</p>
					</Col>
				</Row>
			</Container>
		</div>
		}
		/>
		<Container className="espacamentoAccordion">
			<Row>
				<Col md="12" lg="12"></Col>
			</Row>
		</Container>

		<Accordion
		title={
		<p>
			<b>Quadro 2</b>
		</p>
		}
		content={
		<div className="fundoA11 borda10 espacamento">
			<Container>
				<Row>
					<Col md="12" lg="12">
					<p className="textoUnidade">
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
					</p>
				</Col>
			</Row>
		</Container>
		<Container>
			<Row>
				<Col md="12" lg="12">
				<p className="textoUnidade">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				</p>
			</Col>
		</Row>
		</Container>
		</div>
		}
		/>

		<Container>
			<Row>
				<Col
				md="12"
				lg="12"
				className="espacamentoAccordion"
				></Col>
			</Row>
		</Container>

		<Accordion
		title={
		<p>
			<b>Quadro 3</b>
		</p>
		}
		content={
		<div className="fundoA11 borda10 espacamento">
			<Container>
				<Row>
					<Col md="12" lg="12">
					<p className="textoUnidade">
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
					</p>
				</Col>
			</Row>
		</Container>
		</div>
		}
		/>
		<Container>
			<Row>
				<Col
				md="12"
				lg="12"
				className="espacamentoAccordion"
				></Col>
			</Row>
		</Container>
		<Accordion
		title={
		<p>
			<b>Quadro 4</b>
		</p>
		}
		content={
		<div className="fundoA11 borda10 espacamento">
			<Container>
				<Row>
					<Col md="12" lg="12">
					<p className="textoUnidade">
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
					</p>
				</Col>
			</Row>
		</Container>
		</div>
		}
		/>
		</Col>
		</Row>
		</Container>

		<div className="espacamentoTop35"></div>
		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 7 - Quadro dropwdown dividido"}
		/>
		<div className="espacamentoBottom" id="sec7">
			<DivHide
			textoClique={[
			<b>Clique aqui</b>
			]}
			fundoCaixa={"borda0"}
			textoCaixa={
			<Container>
				<Row>
					<Col
					md="12"
					lg="12"
					className="fundo01bFinal borda10"
					>
					<Row>
						<Col
						md="6"
						lg="6"
						className="bordaPeso"
						>
						<img
						src={imgHeader1}
						className="centerImg iconPesoAltura espacamento"
						/>

						<p className="textoUnidade tituloHeader">
							Peso
						</p>
					</Col>
					<Col md="6" lg="6" className="">
					<img
					src={imgHeader2}
					className="centerImg iconPesoAltura espacamento"
					/>
					<p className="textoUnidade tituloHeader">
						Estatura
					</p>
				</Col>
			</Row>
		</Col>

		<Col
		md="12"
		lg="12"
		className="fundoCinza borda10"
		>
		<Row>
			<Col
			md="6"
			lg="6"
			className="bordaPeso espacamento"
			>
			<ul>
				<li>
					<p>
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
					</p>
				</li>
			</ul>
		</Col>
		<Col
		md="6"
		lg="6"
		className="espacamento"
		>
		<ul>
			<li>
				<p>
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				</p>
			</li>
		</ul>
		</Col>
		</Row>
		</Col>
		</Row>
		</Container>
		}
		/>
		</div>

		{/*<div className="slider">
			<Container>
				<Row>
					<Col
					md="12"
					lg="12"
					className="espacamentoBottom espacamento"
					>
					<Carousel
					activeIndex={activeIndex}
					next={this.next}
					previous={this.previous}
					pause={false}
					ride="carousel"
					interval={false}
					slide={true}
					>
					<CarouselIndicators
					items={items}
					activeIndex={activeIndex}
					onClickHandler={this.goToIndex}
					/>
					{slides}
					<CarouselControl
					direction="prev"
					directionText="Previous"
					onClickHandler={this.previous}
					className="seta"
					/>

					<CarouselControl
					direction="next"
					directionText="Next"
					onClickHandler={this.next}
					className="seta"
					/>
				</Carousel>
			</Col>
		</Row>
		</Container>
		</div>	*/}
		<div id="sec8" className="espacamento"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 8 - Seletor de textos"}
				/>

				<div className="fundoNav">
					<Container className="">
						<Row>
							<Col md="12" lg="12">
								<div className="" id="col-container">
									<Nav tabs className="deslocarNav">
										<Col
											md="3"
											lg="3"
											className="no-gutters col"
										>
											<NavItem className="">
												<NavLink
													className={classnames(
														"cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
														{
															active:
																this.state
																	.activeTab ===
																"1"
														}
													)}
													onClick={() => {
														this.toggle("1");
													}}
												>
													<Col md="12" ls="12">
														<img
															src={clique}
															className="responsive imgClique"
														/>
													</Col>
													<Col
														md="12"
														ls="12"
														className="heightNav"
													>
														<p className="textCenter fontTab">
															<b>Inicial</b>
														</p>
													</Col>
												</NavLink>
											</NavItem>
										</Col>
										<Col
											md="3"
											lg="3"
											className="no-gutters col"
										>
											<NavItem className="">
												<NavLink
													className={classnames(
														"cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
														{
															active:
																this.state
																	.activeTab ===
																"2"
														}
													)}
													onClick={() => {
														this.toggle("2");
													}}
												>
													<Col md="12" ls="12">
														<img
															src={clique}
															className="responsive imgClique"
														/>
													</Col>
													<Col
														md="12"
														ls="12"
														className="heightNav"
													>
														<p className="textCenter fontTab">
															<b>Intermediário</b>
														</p>
													</Col>
												</NavLink>
											</NavItem>
										</Col>

										<Col
											md="3"
											lg="3"
											className="no-gutters col"
										>
											<NavItem>
												<NavLink
													className={classnames(
														" cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
														{
															active:
																this.state
																	.activeTab ===
																"3"
														}
													)}
													onClick={() => {
														this.toggle("3");
													}}
												>
													<Col md="12" ls="12">
														<img
															src={clique}
															className="responsive imgClique"
														/>
													</Col>
													<Col
														md="12"
														ls="12"
														className="heightNav"
													>
														<p className="textCenter fontTab">
															<b>
																Sistematização e
																avaliação
															</b>
														</p>
													</Col>
												</NavLink>
											</NavItem>
										</Col>
									</Nav>
									<div className="espacamentoBottom">
										<TabContent
											activeTab={this.state.activeTab}
											className="fundoAzulNav borda10"
										>
											<TabPane tabId="1">
												<Container>
													<Row>
														<Col md="12" lg="12">
															<Button
																color=""
																className="rightAlign corBotao"
																onClick={() => {
																	this.toggle(
																		"0"
																	);
																}}
															>
																<span className="corX">
																	X
																</span>
															</Button>
														</Col>
														<Col
															md="12"
															lg="12"
															className="esp1"
														>
															<p>
																Você e outros
																membros da
																unidade
																envolvidos no
																grupo realizarão
																a preparação dos
																participantes
																com apresentação
																dos objetivos e
																das atividades.
																Pode ser
																realizada por
																técnicas de
																“relaxamento”
																e/ou
																“aquecimento”,
																com utilização
																de atividades,
																brincadeiras,
																conversas,
																apresentação dos
																participantes do
																grupo e
																descrição de
																principais
																características
																de cada um, etc.
																Podem perguntar
																a idade
																gestacional,
																nome do bebê,
																com quem acham
																que o bebê irá
																se parecer,
																dentre outras
																dinâmicas para
																quebrar o gelo e
																depois
																apresentar os
																objetivos do
																grupo, abrindo
																sempre um espaço
																para sugestão
																pelas
																participantes e
																acompanhantes.
															</p>
														</Col>
													</Row>
												</Container>
											</TabPane>
											<TabPane tabId="2">
												<Container>
													<Row>
														<Col md="12" lg="12">
															<Button
																color=""
																className="rightAlign corBotao"
																onClick={() => {
																	this.toggle(
																		"0"
																	);
																}}
															>
																<span className="corX">
																	X
																</span>
															</Button>
														</Col>
														<Container className="esp1">
															<Row>
																<Col
																	md="12"
																	lg="12"
																>
																	<p className="textoUnidade">
																		Depois,
																		é
																		realizado
																		o
																		desenvolvimento
																		das
																		atividades.
																		Essas
																		devem
																		facilitar
																		a
																		reflexão
																		e a
																		elaboração
																		do tema.
																		Este
																		momento
																		pode ser
																		subdividido
																		em
																		quatro
																		pontos:{" "}
																	</p>{" "}
																</Col>
																<Col md="6" lg="6">
															<p className="textoUnidade">
																a) Utilização de
																técnicas
																lúdicas,
																sensibilização,
																motivação,
																reflexão e
																comunicação.{" "}
															</p>
															<p className="textoUnidade">
																c) Expansão das
																vivências,
																relacionando-as
																com situações do
																cotidiano.{" "}
															</p>
														</Col>
														<Col md="6" lg="6">
															<p className="textoUnidade">
																b) Conversa e
																reflexão sobre
																os sentimentos e
																ideias do grupo
																sobre as
																situações
																vivenciadas.{" "}
															</p>
															<p className="textoUnidade">
																d) Exposição e
																análise das
																informações
																sobre o tema
																comparadas às
																experiências dos
																participantes
																para
																esclarecimentos.{" "}
															</p>
														</Col>
															</Row>
														</Container>

														
													</Row>
												</Container>
											</TabPane>
											<TabPane tabId="3">
												<Container>
													<Row>
														<Col md="12" lg="12">
															<Button
																color=""
																className="rightAlign corBotao"
																onClick={() => {
																	this.toggle(
																		"0"
																	);
																}}
															>
																<span className="corX">
																	X
																</span>
															</Button>
														</Col>
														<Col
															md="12"
															lg="12"
															className="esp1"
														>
															<p>
																Após a
																realização da
																oficina, é
																momento de
																visualizar a
																produção do
																grupo,
																acompanhar o
																desenvolvimento
																da reflexão, do
																crescimento da
																autonomia e
																conhecimento dos
																participantes em
																relação ao tema
																abordado. Os
																participantes do
																grupo devem
																participar da
																tomada de
																decisões sobre
																os próximos
																encontros, ou
																seja, o produto
																da oficina deve
																ser construído
																coletivamente.
															</p>
														</Col>
													</Row>
												</Container>
											</TabPane>
										</TabContent>
									</div>
								</div>
							</Col>
						</Row>
					</Container>
				</div>


		<div id="sec9" className="espacamentoTop35"></div>

		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 9 - Modal"}
		/>

		<div>
			<Container className="fundoIcones2 espacamento">
				<Container className="clickExemplo borda10">
					<Row>
						<Col md="12" lg="12">
						<ModalPopUp
						conceito={
						<p className="espacamento">
							Clique aqui para conhecer um
							exemplo
						</p>
					}
					textoConceito={
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				}
				imgPopUp={
				<img
				src={imgExemplo}
				className="responsive w100 borda10 "
				/>
			}
			></ModalPopUp>
		</Col>
		</Row>
		</Container>

		<Container className="espacamentoBottom">
			<Row>
				<Col md="12" lg="12"></Col>
			</Row>
		</Container>
		</Container>
		</div>

		<div className="espacamento"></div>


		<div id="secINT"></div>
		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 10 - STORYLINE"}
		/>

		<Container className="espacamentoIframe espacamento espacamentoBottom">
			<Row>
				<Col md="12" lg="12">
				<iframe
				src="https://unasus-quali.moodle.ufsc.br/mod/resource/view.php?id=1059"
				width="100%"
				height="700px;"
				frameborder="0"
				scrolling="no"
				className="centerIframe"
				></iframe>
			</Col>
		</Row>
		</Container>

		<div id="sec11" className="espacamento"></div>
		<TituloSubSecaoBarraNova
		tipoBarra={
		this.props.tipoUnidade + " barraAlterada degradeTitulo30"
		}
		titulo={"Animação 11 - VÍDEO NA PÁGINA"}
		/>

		<Container className="espacamento espacamentoBottom ">
			<Row>
				<Col md="12" lg="12">
				<iframe src="https://www.youtube.com/embed/Qc-rbT0goBE" className="video"></iframe>
			</Col>
		</Row>
		</Container>

		{/*<Container>
		 <div id="app">
		      <div id="headline"><h1>React Menu Button</h1></div>
		      <div id="content">
		        <div id="component">
		          <MenuButton {...this.state} elements={ELEMENTS.slice(0, this.state.numElements)}/>
		        </div>
		        <div id="config">
		          <h2>Props</h2>
		          <table>
		            <tbody>
		              <tr>
		                <td>fly out radius:</td>
		                <td><input {...this.getInputProps(NUM, "flyOutRadius")}/></td>
		              </tr>
		              <tr>
		                <td>seperation angle:</td>
		                <td><input {...this.getInputProps(NUM, "seperationAngle")}/></td>
		              </tr>
		              <tr>
		                <td>main button diam:</td>
		                <td><input {...this.getInputProps(NUM, "mainButtonDiam")}/></td>
		              </tr>
		              <tr>
		                <td>child button diam:</td>
		                <td><input {...this.getInputProps(NUM, "childButtonDiam")}/></td>
		              </tr>
		              <tr>
		                <td>num elements:</td>
		                <td><input {...this.getInputProps(NUM, "numElements")}/></td>
		                <td><i className="fa fa-info" onClick={() => 
		                  alert("normaly no number, but an array of obj {icon, onClick}")}/></td>
		              </tr>
		              <tr>
		                <td>stiffness:</td>
		                <td><input {...this.getInputProps(NUM, "stiffness")}/></td>
		              </tr>
		              <tr>
		                <td>damping:</td>
		                <td><input {...this.getInputProps(NUM, "damping")}/></td>
		              </tr>
		              <tr>
		                <td>rotation:</td>
		                <td><input {...this.getInputProps(NUM, "rotation")}/></td>
		              </tr>
		              <tr>
		                <td>main button icon:</td>
		                <td><input {...this.getInputProps(TEX, "mainButtonIcon")}/></td>
		                <td><i className="fa fa-info" onClick={() => 
		                  alert("font awesome icon")}/></td>
		              </tr>
		              <tr>
		                <td>main button icon size:</td>
		                <td><input {...this.getInputProps(TEX, "mainButtonIconSize")}/></td>
		                <td><i className="fa fa-info" onClick={() => 
		                  alert("none | lg | 2x | 3x | 4x | 5x")}/></td>
		              </tr>
		              <tr>
		                <td>child button icon size:</td>
		                <td><input {...this.getInputProps(TEX, "childButtonIconSize")}/></td>
		                <td><i className="fa fa-info" onClick={() => 
		                  alert("none | lg | 2x | 3x | 4x | 5x")}/></td>
		              </tr>
		            </tbody>
		          </table>
		        </div>
		      </div>
		    </div>
		 </Container>*/}

		{/*<Container>
		  <div>
		  	
		  	useFlipAnimation({ root: rootRef, opts, deps: depArray }),
		  </div>
		</Container>*/}


				<div id="sec12" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 12 - SLIDE AUTOMÁTICO"}
				/>

				<Container className="espacamento w60 espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<Carousel auto className="carousel espacamentoBottom20"
								interval={1000}
								duration={2000}
								loop={true}>
								<div style={{backgroundColor: 'tomato', height: '100%'}}>Frame 1</div>
							    <div style={{backgroundColor: 'orange', height: '100%'}}>Frame 2</div>
							    <div style={{backgroundColor: 'orchid', height: '100%'}}>Frame 3</div>
							</Carousel>
						</Col>
					</Row>
				</Container>

				<div id="sec13" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 13 - MENU DROPDOWN COM PESQUISA DE ITENS"}
				/>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<TreeMenu
							  data={[
							    {
							      key: 'mammal',
							      label: 'Primeiro nível - 1',
							      className: 'textoUnidade',
							      nodes: [
							        {
							          key: 'canidae',
							          label: 'Segundo nível - 1',
							          nodes: [
							            {
							              key: 'dog',
							              label: 'Terceiro nível - 1',
							              nodes: [],
							            },
							            {
							              key: 'fox',
							              label: 'Terceiro nível - 2',
							              nodes: [],
							            },
							            {
							              key: 'wolf',
							              label: 'Terceiro nível - 3',
							              nodes: [],
							            }
							          ],
							        }
							      ],
							    },
							    {
							      key: 'reptile',
							      label: 'Primeiro nível - 2',
							      nodes: [
							        {
							          key: 'squamata',
							          label: 'Segundo nível - 1',
							          nodes: [
							            {
							              key: 'lizard',
							              label: 'Terceiro nível - 1',
							            },
							            {
							              key: 'snake',
							              label: 'Terceiro nível - 3',
							            },
							            {
							              key: 'gekko',
							              label: 'Terceiro nível - 3',
							            }
							          ],
							        }
							      ],
							    }
							  ]}
							  hasSearch={true}
							  debounceTime={125}
							  disableKeyboard={false}
							  resetOpenNodesOnDataUpdate={false}
							/>
						</Col>
					</Row>
				</Container>


				<div id="sec14" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 14 - PULSE"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="pulse"
							animationInDuration={5000}>	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>
				
				<div id="sec15" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 15 - HEADSHAKE"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="headShake">	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>

				<div id="sec16" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 16 - BOUNCE IN"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="bounceIn" duration={3}>	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>

				<div id="sec17" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 17 - BOUNCE IN LEFT"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="bounceInLeft">	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>

				<div id="sec18" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 18 - ROTATE-IN UP-LEFT"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="rotateInUpLeft" duration={3}>	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>

				<div id="sec19" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 19 -  ZOOM-IN"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="zoomIn" duration={3}>	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>

				<div id="sec20" className="espacamento"></div>
				<TituloSubSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 20 - SLIDE-IN LEFT"}
				/>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<ScrollAnimation animateIn="slideInLeft" duration={3}>	
								<img className="w40 centerImg" src={img01}/>
							</ScrollAnimation>

						</Col>
					</Row>
				</Container>

				

				{/*<div id="sec21"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 21 - DRAGGER"}
				/>

				<Container className="espacamento espacamentoBottom">
					<Row>
						<Dragact
							layout={fakeData}//必填项
							col={16}//必填项
							width={800}//必填项
							rowHeight={40}//必填项
							margin={[5, 5]}//必填项
							className='plant-layout centerImg'//必填项
							style={{ background: '#333' }}//非必填项
							placeholder={true}//非必填项
							>
							{(item, provided) => {
								return (
								<div
								{...provided.props}
								{...provided.dragHandle}
								style={{
								...provided.props.style,
								...getblockStyle(provided.isDragging)
							}}
							>
							{provided.isDragging ? 'Em movimento' : 'Quadro estacionado :deslize para para qualquer posicao do espaço preto'}
						</div>
						)
					   }}
					  </Dragact>

					</Row>  
				</Container> */}


				<div id="secEST"></div>
				<TituloSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Estáticos"}
				/>

				<div id="sec22"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 22 - Imagem de Fundo"}
				/>

				<Container className="fundoImg">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</Col>
					</Row>
				</Container>
				
				<div id="sec21"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 21 - Faixa em degrade"}
				/>

				<div className="fundoA">
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</Col>
					</Row>
				</Container>
				</div>
				
				<div id="sec22" className="espacamento"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 22 - Imagem de Fundo Lateral"}
				/>

				<div className="fundoTransparencia">
				<Container className="">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</Col>
					</Row>
				</Container>
				</div>
				
				<div id="sec23" className="espacamento"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 23 - Fundo Lateral"}
				/>
				<div className="fundo2-01 espacamento espacamentoBottom">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
							nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
							eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
							</Col>
						</Row>
					</Container>
				</div>

				<div id="sec24" className="espacamento"></div>
				<TituloSubSecaoBarraNova
				tipoBarra={
				this.props.tipoUnidade + " barraAlterada degradeTitulo30"
				}
				titulo={"Animação 24 - Figura de Respiro"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
						incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
						nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
						eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				
						<img src={img13} className="responsive w100"/>
							
						<p className="textoUnidade espacamento">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
						incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
						nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
						eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</Col>
					</Row>
				</Container>

				<div className="espacamentoTop35" id="sec25"></div>
					<TituloSubSecaoBarraNova
					tipoBarra={
					this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 25 - Imagem de fundo vazado"}
					/>
					<div id="imgRolagemDeFundo1">
						<Container>
							<Row>
								<Col
								md="11"
								lg="11"
								className="caixaFundo borda10 espacoLeftFundoAzul"
								>
								<p className="textoUnidade espacamento2x espacamentoLateral">
									"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."{" "}
								</p>
							</Col>
						</Row>
					</Container>
					</div>

					<div className="espacamentoTop35" id="sec25"></div>
					<TituloSubSecaoBarraNova
					tipoBarra={
					this.props.tipoUnidade + " barraAlterada degradeTitulo30"
					}
					titulo={"Animação 25 - Imagem de fundo vazado"}
					/>
					<Container className="faixa01 espacamentoBottom espacamento">
						<Row>
							<Col
							md="11"
							lg="11"
							className="borda10"
							>
								<p className="textoUnidade espacamento2x espacamentoLateral">
									"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."{" "}
								</p>
							</Col>
						</Row>
					</Container>






			</div>

		);
	}
}

const styleLogo = {
bottom: "50%",
left: "50%",
marginTop: "20%"
};

const Caixa1 = () => (
<Container className="espacamentoBottom">
	<Row>
		<Col md="12" lg="12">
		<CaixaTemplate
		textoClique={"clicando no ícone"}
		classImg={"iconeTexto12_2"}
		tipoCaixa={"caixaDestaque"}
		icone={iconeDestaque}
		textoUnidade={
		"Como tornar hábito uma alimentação saudável, utilizando os recursos econômicos disponíveis e estimulando o consumo dos alimentos produzidos localmente?"
	}
	inserirLink={false}
	proporcaoCol1={1}
	proporcaoCol2={11}
	></CaixaTemplate>
</Col>
</Row>
</Container>
);

const Caixa2 = () => (
<Container>
	<Row>
		<Col md="12" lg="12">
		<CaixaTemplate
		textoClique={"clicando no ícone"}
		classImg={"iconeTexto12_2"}
		tipoCaixa={"caixaReflexao"}
		icone={iconeReflexao}
		textoUnidade={
		"e acesse outras informações sobre os critérios para classificação do estado nutricional, de acordo com os índices antropométricos da criança."
	}
	inserirLink={true}
	link={
	"http://bvsms.saude.gov.br/bvs/publicacoes/estrategias_cuidado_doenca_cronica_obesidade_cab38.pdf"
}
inserirLinkInicio={true}
textoLink={"Clique aqui "}
proporcaoCol1={1}
proporcaoCol2={11}
></CaixaTemplate>
</Col>
</Row>
</Container>

);



{/* <div className="fundoNav">
	<Container className="">
		<Row>
			<Col md="12" lg="12">
			<div className="" id="col-container">
				<Nav tabs className="deslocarNav">
					<Col
					md="3"
					lg="3"
					className="no-gutters col"
					>
					<NavItem className="">
						<NavLink
						className={classnames(
						"cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
						{
							active:
							this.state
							.activeTab ===
							"1"
						}
						)}
						onClick={() => {
						this.toggle("1");
					}}
					>
					<Col md="12" ls="12">
					<img
					src={clique}
					className="responsive imgClique"
					/>
				</Col>
				<Col
				md="12"
				ls="12"
				className="heightNav"
				>
				<p className="textCenter fontTab">
					<b>Inicial</b>
				</p>
			</Col>
		</NavLink>
	</NavItem>
</Col>
<Col
md="3"
lg="3"
className="no-gutters col"
>
<NavItem className="">
	<NavLink
	className={classnames(
	"cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
	{
		active:
		this.state
		.activeTab ===
		"2"
	}
	)}
	onClick={() => {
	this.toggle("2");
}}
>
<Col md="12" ls="12">
<img
src={clique}
className="responsive imgClique"
/>
</Col>
<Col
md="12"
ls="12"
className="heightNav"
>
<p className="textCenter fontTab">
	<b>Intermediário</b>
</p>
</Col>
</NavLink>
</NavItem>
</Col>

<Col
md="3"
lg="3"
className="no-gutters col"
>
<NavItem>
	<NavLink
	className={classnames(
	" cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
	{
		active:
		this.state
		.activeTab ===
		"3"
	}
	)}
	onClick={() => {
	this.toggle("3");
}}
>
<Col md="12" ls="12">
<img
src={clique}
className="responsive imgClique"
/>
</Col>
<Col
md="12"
ls="12"
className="heightNav"
>
<p className="textCenter fontTab">
	<b>
		Sistematização e
		avaliação
	</b>
</p>
</Col>
</NavLink>
</NavItem>
</Col>
</Nav>
<div className="espacamentoBottom">
	<TabContent
	activeTab={this.state.activeTab}
	className="fundoAzulNav borda10"
	>
	<TabPane tabId="1">
		<Container>
			<Row>
				<Col md="12" lg="12">
				<Button
				color=""
				className="rightAlign corBotao"
				onClick={() => {
				this.toggle(
				"0"
				);
			}}
			>
			<span className="corX">
				X
			</span>
		</Button>
	</Col>
	<Col
	md="12"
	lg="12"
	className="esp1"
	>
	<p>
		Você e outros
		membros da
		unidade
		envolvidos no
		grupo realizarão
		a preparação dos
		participantes
		com apresentação
		dos objetivos e
		das atividades.
		Pode ser
		realizada por
		técnicas de
		“relaxamento”
		e/ou
		“aquecimento”,
		com utilização
		de atividades,
		brincadeiras,
		conversas,
		apresentação dos
		participantes do
		grupo e
		descrição de
		principais
		características
		de cada um, etc.
		Podem perguntar
		a idade
		gestacional,
		nome do bebê,
		com quem acham
		que o bebê irá
		se parecer,
		dentre outras
		dinâmicas para
		quebrar o gelo e
		depois
		apresentar os
		objetivos do
		grupo, abrindo
		sempre um espaço
		para sugestão
		pelas
		participantes e
		acompanhantes.
	</p>
</Col>
</Row>
</Container>
</TabPane>
<TabPane tabId="2">
	<Container>
		<Row>
			<Col md="12" lg="12">
			<Button
			color=""
			className="rightAlign corBotao"
			onClick={() => {
			this.toggle(
			"0"
			);
		}}
		>
		<span className="corX">
			X
		</span>
	</Button>
</Col>
<Container className="esp1">
	<Row>
		<Col
		md="12"
		lg="12"
		>
		<p className="textoUnidade">
			Depois,
			é
			realizado
			o
			desenvolvimento
			das
			atividades.
			Essas
			devem
			facilitar
			a
			reflexão
			e a
			elaboração
			do tema.
			Este
			momento
			pode ser
			subdividido
			em
			quatro
			pontos:{" "}
		</p>{" "}
	</Col>
	<Col md="6" lg="6">
	<p className="textoUnidade">
		a) Utilização de
		técnicas
		lúdicas,
		sensibilização,
		motivação,
		reflexão e
		comunicação.{" "}
	</p>
	<p className="textoUnidade">
		c) Expansão das
		vivências,
		relacionando-as
		com situações do
		cotidiano.{" "}
	</p>
</Col>
<Col md="6" lg="6">
<p className="textoUnidade">
	b) Conversa e
	reflexão sobre
	os sentimentos e
	ideias do grupo
	sobre as
	situações
	vivenciadas.{" "}
</p>
<p className="textoUnidade">
	d) Exposição e
	análise das
	informações
	sobre o tema
	comparadas às
	experiências dos
	participantes
	para
	esclarecimentos.{" "}
</p>
</Col>
</Row>
</Container>


</Row>
</Container>
</TabPane>
<TabPane tabId="3">
	<Container>
		<Row>
			<Col md="12" lg="12">
			<Button
			color=""
			className="rightAlign corBotao"
			onClick={() => {
			this.toggle(
			"0"
			);
		}}
		>
		<span className="corX">
			X
		</span>
	</Button>
</Col>
<Col
md="12"
lg="12"
className="esp1"
>
<p>
	Após a
	realização da
	oficina, é
	momento de
	visualizar a
	produção do
	grupo,
	acompanhar o
	desenvolvimento
	da reflexão, do
	crescimento da
	autonomia e
	conhecimento dos
	participantes em
	relação ao tema
	abordado. Os
	participantes do
	grupo devem
	participar da
	tomada de
	decisões sobre
	os próximos
	encontros, ou
	seja, o produto
	da oficina deve
	ser construído
	coletivamente.
</p>
</Col>
</Row>
</Container>
</TabPane>
</TabContent>
</div>
</div>
</Col>
</Row>
</Container>
</div>


*/}

export default Unidade;

/*

class MenuButton extends React.Component {
  
  constructor(props){
    super(props);
    
    this.state = {
      isOpen: true
    };
  }
  
  toggleMenu(){
    let { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen
    });
  }
  
  getMainButtonStyle(){
    let { mainButtonDiam } = this.props;
    return {
      width: mainButtonDiam,
      height: mainButtonDiam
    }
  }
  
  getInitalChildButtonStyle(){
    let { childButtonDiam, mainButtonDiam, stiffness, damping } = this.props;
    return {
      width: childButtonDiam,
      height: childButtonDiam,
      zIndex: -1,
      top: spring(mainButtonDiam/2 - childButtonDiam/2, {stiffness, damping}),
      left: spring(mainButtonDiam/2 - childButtonDiam/2, {stiffness, damping})
    }
  }
  
  getFinalChildButtonStyle(index){
    let { childButtonDiam, mainButtonDiam, stiffness, damping } = this.props;
    let { deltaX, deltaY } = this.getFinalDeltaPositions(index);
    return {
      width: childButtonDiam,
      height: childButtonDiam,
      zIndex: spring(0),
      top: spring(mainButtonDiam/2 + deltaX, {stiffness, damping}),
      left: spring(mainButtonDiam/2 - deltaY, {stiffness, damping})
    }
  }
  
  getFinalDeltaPositions(index) {
    let NUM_CHILDREN = this.props.elements.length;
    let CHILD_BUTTON_DIAM = this.props.childButtonDiam;
    let FLY_OUT_RADIUS = this.props.flyOutRadius;
    let SEPARATION_ANGLE = this.props.seperationAngle;
    let ROTATION = this.props.rotation;
    let FAN_ANGLE = (NUM_CHILDREN - 1) * SEPARATION_ANGLE;
    let BASE_ANGLE = ((180 - FAN_ANGLE)/2)+90+ROTATION;
    
    let TARGET_ANGLE = BASE_ANGLE + ( index * SEPARATION_ANGLE );
    return {
      deltaX: FLY_OUT_RADIUS * Math.cos(toRadians(TARGET_ANGLE)) - (CHILD_BUTTON_DIAM/2),
      deltaY: FLY_OUT_RADIUS * Math.sin(toRadians(TARGET_ANGLE)) + (CHILD_BUTTON_DIAM/2)
    };
  }
  
  getCProps(){
    return {
      mainButtonProps: () => ({
        className: "button-menu",
        style: this.getMainButtonStyle(),
        onClick: ::this.toggleMenu
      }),
      childButtonProps: (style, onClick) => ({
        className: "button-child",
        style,
        onClick
      }),
      childButtonMotionProps: (index, isOpen) => ({
        key: index,
        style: isOpen ? ::this.getFinalChildButtonStyle(index)
                      : ::this.getInitalChildButtonStyle()
      }),
      // handle Icons
      childButtonIconProps: (name) => ({
        className: "child-button-icon fa fa-"+name+" fa-"+this.props.childButtonIconSize
      }),
      mainButtonIconProps: (name) => ({
        className: "main-button-icon fa fa-"+name+" fa-"+this.props.mainButtonIconSize
      })
    }
  }
  
  renderChildButton(item, index){
    let { isOpen } = this.state;
    let cp = this.getCProps();
    
    //return <div {...cp.childButtonProps(index, isOpen)}/>;
    
    return <Motion {...cp.childButtonMotionProps(index, isOpen)}>
      {
        (style) => <div {...cp.childButtonProps(style, item.onClick)}>
          <i {...cp.childButtonIconProps(item.icon)}/>
        </div>
      }
    </Motion>;
  }
  
  render(){
    let cp = this.getCProps();
    let { elements, mainButtonIcon } = this.props;
    let { isOpen } = this.state;
    
    return <div className="button-container">
      { elements.map((item, i) => this.renderChildButton(item, i)) }
      <div {...cp.mainButtonProps()}>
        <i {...cp.mainButtonIconProps(mainButtonIcon)}/>
      </div>
    </div>;
  }
}*/
