import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from "reactstrap";
import { caixa } from "./Utilitarios.js";
import { criarLink } from "./Utilitarios.js";
import { criarIcone } from "./Utilitarios.js";
import { criarIconeHide } from "./Utilitarios.js";
import ModalPopUp from "./ModalPopUp.js";

import {
	TituloSecaoBarra2,
	TituloSubSecaoBarra,
	CaixaTemplate,
	CaixaGeralRetorno,
	TituloSecaoBarraNova,
	TituloSubSecaoBarraNova
} from "./Utilitarios.js";

import "./css/Modal.css";

import arrow from "../img/bullet.png";

import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import iconeLink from "../img/link.png";
import circle1 from "../img/circle1.png";
import circle2 from "../img/circle2.png";
import circle3 from "../img/circle3.png";
import img01 from "../img/uni1/img01.jpg";
import img001 from "../img/un1/img01.png";
import imgInicial from "../img/info01.png";
import img02 from "../img/un1/img02.png";
import img03 from "../img/un1/img03.png";
import img04 from "../img/un1/img04.png";
import img05 from "../img/un1/img05.png";
import img4 from "../img/info00.png";
import img5 from "../img/info00.png";
import final from "../img/santaCeia.jpg";
import imgFundo01 from "../img/frutas.jpeg";
import imgSaude from "../img/saude-da-familia.jpg";
import imgCapivara from "../img/capivaras.jpeg";
import imgRegador from "../img/imgRegador.jpg";
import carta from "../img/carta.png";
import equipe from "../img/equipe.jpeg";
import info01 from "../img/infoTabela01.png";
import info02 from "../img/infoTabela02.png";
import info05 from "../img/uni1/infoOnline-34.png";
import infoIMC from "../img/uni1/info01.png";
import infoCirculos from "../img/uni1/infoCirculos.png";
import imgMobilidade from "../img/uni1/fundoMobilidade.png";
import imgFeira from "../img/uni1/feiraOrganica.jpeg";
import imgQuebraCabeca from "../img/uni1/infoQuebraCabeca.png";
import imgRede from "../img/uni1/redeFinal.png";
import info07 from "../img/uni1/infoMapa.png";
import jantar from "../img/jantar.jpg";
import imgJantar from "../img/mesaJantar.jpg";
import imgSalada from "../img/salada.png";
import imgFases from "../img/fasesFinal.png";
import imgPes from "../img/pes.jpeg";
import ScrollAnimation from "react-animate-on-scroll";
import iconeVideo from "../img/video.png";
import pratos from "../img/pratos.png";
import academiaSaude from "../img/academiaSaude.jpg";
import testeiraInfografico from "./../img/testeiraInfografico.png";
import click from "../img/click.png";
import cliqueAqui from "../img/clique.png";
import imgBola from "../img/PDF_MOOC2V3-10.png";
import imgTab from "../img/infoTabela.png";
import infoTabela1 from "../img/uni1/infoTabela1.png";

import imgIMC1 from "../img/uni1/infoIMC1.png";
import imgIMC2 from "../img/uni1/infoIMC4.png";

import IconeHide from "./IconeNota.js";

const items = [
	{
		src: img02
	},
	{
		src: img03
	}
];

var styleInfografico = {
	backgroundImage: { testeiraInfografico },
	backgroundSize: "cover"
};

class Unidade extends React.Component {
	constructor() {
		super();
		this.state = { activeIndex: 0, isHiddenCaixa: true };
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}
	toggleCaixa() {
		this.setState({
			isHiddenCaixa: !this.state.isHiddenCaixa
		});
	}

	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === items.length - 1
				? 0
				: this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === 0
				? items.length - 1
				: this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}

	render() {
		const { activeIndex } = this.state;

		const slides = items.map(item => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={item.src}
				>
					<img
						src={item.src}
						alt={item.altText}
						className="responsive w55 centerImg"
					/>
				</CarouselItem>
			);
		});
		return (
			<div className="">
				<TituloSecaoBarraNova
					titulo={"Introdução"}
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento espacamentoBottom degradeTitulo25"
					}
				/>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Esta unidade abordará a problemática do
								sobrepeso e da obesidade como uma questão
								relevante de saúde pública, com dimensões
								multifatoriais e que necessita de atuação
								multiprofissional e intersetorial para a atenção
								e cuidado integral às pessoas, com base nos
								princípios e diretrizes da Atenção Básica (AB),
								reconhecendo as potencialidades e desafios do
								território para prevenção e controle da
								obesidade.
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento espacamentoBottom degradeTitulo3"
					}
					titulo={"DEFINIÇÕES DE SOBREPESO E OBESIDADE"}
				/>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Em primeiro lugar, é preciso diferenciar os
								termos sobrepeso e obesidade, muitas vezes
								utilizados como sinônimos. Enquanto o primeiro
								significa um aumento exclusivo do peso, o
								segundo representa o aumento da adiposidade
								(gordura) corporal. Para esclarecer esses
								conceitos, define-se obesidade como uma condição
								crônica que se caracteriza pelo acúmulo
								excessivo de gordura a um nível tal que a saúde
								esteja comprometida (CUPPARI, 2014). Ou seja, a
								obesidade é um agravamento do sobrepeso, que
								acarreta em problema de saúde. Aprofunde um
								pouco mais seus conhecimentos sobre a obesidade{" "}
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"A obesidade faz parte do grupo de doenças crônicas não transmissíveis (DCNT). As DCNTs podem ser caracterizadas pela sua história natural prolongada, múltiplos fatores de risco, tendo curso assintomático e em geral lento, prolongado e permanente, com períodos de remissão e de exacerbação, lesões celulares irreversíveis e evolução para diferentes graus de incapacidade ou para a morte (PINHEIRO; FREITAS; CORSO, 2004)."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Usualmente, o sobrepeso e a obesidade são
								determinados pelo Índice de Massa Corporal
								(IMC), calculado por meio da divisão do peso em
								kg pela altura em metros elevada ao quadrado
								(kg/m²), sendo esse o cálculo mais usado para
								avaliação da adiposidade corporal – um bom
								indicador: simples, prático e sem custo.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img001}
								className="responsive w60 centerImg"
							/>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row></Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Além do excesso de gordura corporal, é
								importante considerar ainda sua distribuição
								regional, pois o excesso de gordura localizada
								na região abdominal (barriga) significa aumento
								no risco de comorbidade (doenças associadas).
								Então, de acordo com a distribuição de gordura,
								a obesidade pode ser caracterizada em (CUPPARI,
								2014):
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								<b>Obesidade androide</b>: lembra o formato de
								uma maçã, pois a gordura está mais concentrada
								na região abdominal. Sua presença está
								relacionada com alto risco cardiovascular.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								<b>Obesidade ginoide</b>: é o depósito aumentado
								de gordura nos quadris, semelhante a uma pera.
								Sua presença está relacionada com um risco maior
								de artroses e varizes.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								É fundamental que você, profissional de saúde,
								reconheça a obesidade e promova reflexão sobre o
								cuidado e acompanhamento dos usuários na Atenção
								Básica. Mediante a isso, nos próximos itens
								conheceremos alguns aspectos que estão
								fortemente relacionados com a obesidade, e que
								precisam ser conhecidos para subsidiar nossas
								ações no território ampliado. Vamos lá!{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"DETERMINAÇÃO SOCIAL DO SOBREPESO E DA OBESIDADE"}
				/>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A obesidade tem causas multifatoriais e resulta
								de interação de fatores genéticos, metabólicos,
								sociais, comportamentais, ambientais e
								culturais. Assim, é necessário analisar as
								relações dessa doença com seus determinantes
								sociais: fatores sociais, econômicos, culturais,
								étnico/raciais, psicológicos e comportamentais
								que influenciam a ocorrência de problemas de
								saúde e seus fatores de risco na população.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Assim, alguns indicadores servem para embasar as
								ações de promoção da saúde na Atenção Básica,
								entretanto é importante identificá-los nos
								contextos populacionais, direcionando para a
								atenção à saúde da pessoa obesa (BRASIL, 2006).{" "}
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaReflexao"}
									icone={iconeReflexao}
									textoUnidade={
										"Quais são os fatores que podem levar ao excesso de peso e à obesidade? Existem outras doenças associadas? "
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Ao longo dos anos, ocorreram mudanças nos
								padrões alimentares da população (transição
								nutricional) que estão fortemente ligadas também
								às modificações de ordem demográfica e social.
								Fatores sociais, econômicos e culturais estão
								presentes, destacando-se o novo papel feminino
								na sociedade e sua inserção no mercado de
								trabalho, a concentração das populações no meio
								urbano e a diminuição do esforço físico e,
								consequentemente, do gasto energético, tanto no
								trabalho quanto na rotina diária, assim como a
								crescente industrialização dos alimentos (MOTTA
								et al., 2004).
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								O padrão de consumo alimentar atual está baseado
								na excessiva ingestão de alimentos de alta
								densidade energética, ricos em açúcares simples,
								gordura saturada, sódio e conservantes, e pobres
								em fibras e micronutrientes. Os principais
								responsáveis pelo aumento acelerado da obesidade
								no mundo e em nosso país são relacionados ao
								ambiente e às mudanças de modo de vida, sendo,
								portanto, passíveis de intervenção demandando
								ações no âmbito individual e coletivo.{" "}
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img01}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>
				<Container className="">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A influência do ambiente em que as pessoas
								convivem pode contribuir para a epidemia da
								obesidade, pois, quando tais ambientes
								incentivam a inatividade física e as escolhas
								alimentares não saudáveis, eles são
								caracterizados como obesogênicos e, por isso,
								responsáveis por promoverem essa doença
								(MENDONÇA; ANJOS, 2004).{" "}
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaReflexao"}
									icone={iconeReflexao}
									textoUnidade={
										"Você já observou, em seu território, se existem espaços que estimulam a adoção de hábitos de vida saudável nos usuários, tais como praças, academias ao ar livre, parques, espaços de lazer, feiras, supermercados, entre outros?"
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>
							</p>
						</Col>
					</Row>
				</Container>
				<Container className="">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Um fator imprescindível para a promoção da
								prática regular de atividade física se refere às
								condições de infraestrutura, pois a utilização
								de espaços públicos seguros facilita a
								incorporação dessa prática no cotidiano. A
								segurança nas ruas é um fator essencial para a
								garantia desses espaços, assim como o
								planejamento urbano, devendo prever instalações
								para iluminação, recreação, ciclovias, condições
								de calçadas, investimento em parques e
								equipamentos públicos (BRASIL, 2006).
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Além disso, o nível de escolaridade e a renda
								têm sido identificados como variáveis que podem
								interferir na forma como a população escolhe
								seus alimentos, na adoção de comportamentos
								saudáveis e na interpretação das informações
								sobre cuidados para a saúde. Mudanças
								sociocomportamentais da população também estão
								implicadas no aumento da ingestão alimentar e,
								portanto, no aparecimento da obesidade, tais
								como: a diminuição do número de refeições
								realizadas em casa, o aumento compensatório da
								alimentação em redes de fast food levando ao
								aumento do conteúdo calórico de cada refeição.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Assim, a obesidade é determinada por múltiplos
								fatores, e por isso fica tão complexo e difícil
								definir uma única estratégia para combatê-la.
								Apontar um fator dietético específico como
								responsável pelo ganho de peso é excluir todo o
								ambiente, os hábitos de vida e a maneira com que
								nos relacionamos com a alimentação, que, como
								visto, são um potente estímulo para o ganho de
								peso e obesidade. Como profissionais da saúde,
								de que maneira podemos auxiliar os usuários com
								intervenções em nível local e incentivá-los na
								adoção de hábitos saudáveis?
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"EPIDEMIOLOGIA DO SOBREPESO E DA OBESIDADE"}
				/>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O panorama da evolução nutricional da população
								brasileira revela, nas últimas décadas, mudanças
								em seu padrão.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							{/*<p>
								<ul>
									<li>
										mudanças em alguns momentos da vida
										(exemplo: casamento, viuvez, separação),{" "}
									</li>
									<li>
										determinadas situações de violência,
										fatores psicológicos (como o estresse, a
										ansiedade, a depressão e a compulsão
										alimentar),{" "}
									</li>
									<li>
										específicas fases da vida (fase
										intrauterina, o peso de nascimento, a
										amamentação, a fase de rebote do peso no
										período de aumento do peso que ocorre
										entre os 5 e 7 anos de idade, fase
										puberal e menopausa),{" "}
									</li>
									<li>
										aumento da idade das grávidas (o risco
										de obesidade na criança aumenta cerca de
										15% para cada aumento de 5 anos na idade
										materna),
									</li>
									<li>
										alguns tratamentos medicamentosos
										(psicofármacos e corticoides),{" "}
									</li>
									<li>a suspensão do hábito de fumar, </li>
									<li>o consumo excessivo de álcool,</li>
									<li>
										a redução drástica de atividade física.
									</li>
								</ul>
							</p>*/}
							<img
								src={img04}
								className="responsive w100 centerImg borda10"
							/>
						</Col>
					</Row>
				</Container>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As tendências temporais da desnutrição e da
								obesidade definem uma das características
								marcantes do processo de transição nutricional
								do país. Ao mesmo tempo em que declina a
								ocorrência da desnutrição em crianças e adultos
								em ritmo bem acelerado, aumenta a prevalência de
								sobrepeso e obesidade. No entanto, esses agravos
								continuam a coexistir, ainda que a desnutrição
								atinja grupos populacionais mais delimitados
								(populações mais vulneráveis), representando
								situação de extrema gravidade social.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Dados recentes da Vigitel – inquérito telefônico
								para vigilância de fatores de risco e proteção
								para doenças crônicas do Ministério da Saúde –
								apontam que, no Brasil, a frequência de adultos
								com excesso de peso ocorre de maneira diferente
								em cada estado, e com diferenças expressivas de
								acordo com o sexo, variando entre 47,7% em
								Palmas e 60,6% em Rio Branco (BRASIL, 2017).
								Acompanhe, a seguir, outros dados apresentados
								na pesquisa.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="slider">
					<Container>
						<Row>
							<Col
								md="12"
								lg="12"
								className="espacamentoBottom espacamento"
							>
								<Carousel
									activeIndex={activeIndex}
									next={this.next}
									previous={this.previous}
									pause={false}
									ride="carousel"
									interval={false}
									slide={true}
								>
									<CarouselIndicators
										items={items}
										activeIndex={activeIndex}
										onClickHandler={this.goToIndex}
									/>
									{slides}
									<CarouselControl
										direction="prev"
										directionText="Previous"
										onClickHandler={this.previous}
										className="seta"
									/>
									{/*TODO: colocar seta aqui*/}
									<CarouselControl
										direction="next"
										directionText="Next"
										onClickHandler={this.next}
										className="seta"
									/>
								</Carousel>
							</Col>
						</Row>
					</Container>
				</div>

				{/*<Container className="espacamentoBottom">
					<Row>
						<Col md="6" lg="6">
							<img src={img02} className="responsive w100" />
						</Col>
						<Col md="6" lg="6">
							<img src={img03} className="responsive w100" />
						</Col>
					</Row>
				</Container>*/}
				{/*<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Maiores frequências de excesso de peso:
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Homens </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Rio Branco (65,8%),
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Cuiabá (62,1%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Porto Alegre (62,1%)
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Mulheres</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Rio Branco (55,8%),{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Campo Grande (54,5%){" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Salvador (54,1%). </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Menores frequências de excesso de peso:
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Homens</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Distrito Federal (50,6%)
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- São Luís (50,9%) </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Goiânia (52,4%) </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Mulheres</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Palmas (41,7%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Florianópolis (42,1%){" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Goiânia (45,1%).</p>
						</Col>
					</Row>
				</Container>*/}
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Essa condição tendeu a aumentar com a idade até
								os 64 anos. Entre as mulheres, a frequência de
								excesso de peso diminuiu com o aumento do nível
								de escolaridade. Já para a obesidade, a
								frequência de adultos obesos variou entre 14,5%
								em Florianópolis e 23,8% em Rio Branco. Em ambos
								os sexos, a frequência da obesidade aumenta duas
								vezes da faixa de 18 a 24 anos para a faixa de
								25 a 34 anos de idade. A frequência de obesidade
								diminui com o aumento da escolaridade.
							</p>
						</Col>
					</Row>
				</Container>
				{/*<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Maiores frequências de obesidade:{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Homens</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Rio Branco (24,8%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- João Pessoa (23,8%){" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Cuiabá (23,0%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Mulheres</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Rio Branco (22,8%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Maceió (22,5%) </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Salvador (21,7%). </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Menores frequências de obesidade:{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Homens</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- São Luís (12,5%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Vitória (12,6%) </p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Florianópolis (14,1%){" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">Mulheres</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Goiânia (14,5%)</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								- Florianópolis (14,7%){" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">- Palmas (14,8%).</p>
						</Col>
					</Row>
				</Container>*/}
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Essa pesquisa também avaliou o consumo alimentar
								de frutas e hortaliças da população brasileira,
								em que se considera como consumo regular a
								ingestão desses alimentos em cinco ou mais dias
								da semana. Os resultados mostraram que a
								frequência de adultos que consomem cinco ou mais
								porções diárias de frutas e hortaliças foi baixa
								na maioria das cidades estudadas, variando entre
								15,8% em Rio Branco e 35,5% no Distrito Federal.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p>
								As maiores frequências foram encontradas, entre
								homens, no Distrito Federal (32,0%), em Belo
								Horizonte (25,0%) e Vitória (23,9%) e, entre
								mulheres, no Distrito Federal (38,5%), em Belo
								Horizonte e em Goiânia (36,2%). As menores
								frequências no sexo masculino ocorreram em Rio
								Branco (12,2%), Macapá (13,5%) e Belém (14,2%),
								e, no sexo feminino, em Rio Branco (19,2%),
								Belém (19,5%) e Fortaleza (19,6%).{" "}
							</p>
						</Col>
						<Col md="6" lg="6">
							<p>
								O panorama final mostrou que a frequência de
								consumo recomendado de frutas e hortaliças foi
								de 24,4%, sendo menor em homens (19,4%) do que
								em mulheres (28,7%). Em ambos os sexos, a
								frequência do consumo recomendado de frutas e
								hortaliças aumentou com a idade e com a
								escolaridade.{" "}
								<b
									className="cursorIcone"
									onClick={this.toggleCaixa.bind(this)}
								>
									clicando no ícone{" "}
									<img
										src={iconeSaibaMais}
										className={"iconeTexto6"}
									/>
								</b>
								{/*<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaSaibaMais"}
									icone={iconeSaibaMais}
									textoUnidade={
										"Confira também informações sobre o consumo alimentar de carnes com excesso de gordura, leite com teor integral de gordura, alimentos doces em cinco ou mais dias da semana, refrigerantes, feijão, substituição de almoço e jantar por lanches, prática de atividade física, "
									}
									link={
										"http://www.abeso.org.br/uploads/downloads/100/5949633674659.pdf"
									}
									inserirLink={true}
									textoLink={"acesse clicando aqui."}
									inserirLinkInicio={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>*/}
							</p>
						</Col>
					</Row>
				</Container>

				{!this.state.isHiddenCaixa && <Caixa1 />}

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Perceba, a partir dos dados apresentados, a
								relevância da obesidade como problema de saúde
								pública em nosso país, e como a prevalência do
								sobrepeso e da obesidade se comporta de maneira
								diferente em homens e mulheres. Lembre-se de que
								as diferenças de saúde entre ambos denotam
								fatores biológicos, mas também refletem
								comportamentos específicos.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								No item seguinte vamos conhecer alguns problemas
								que a obesidade pode causar com os custos
								diretos e indiretos para o sistema de saúde no
								Brasil. Vamos lá!{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={
						"IMPACTOS E CONSEQUÊNCIAS DO SOBREPESO E DA OBESIDADE"
					}
				/>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O excesso de peso é um dos principais fatores
								que contribuem para a mortalidade e para a carga
								global de doenças no mundo. Em termos de mortes
								atribuíveis, os principais fatores de risco são
								a hipertensão arterial (contribui para 13% das
								mortes no mundo), seguida do consumo de tabaco
								(9%), hiperglicemia (6%), inatividade física
								(6%) e excesso de peso ou obesidade (5%) (OMS,
								2009). Em 2007, 72% das mortes foram atribuídas
								às DCNT (SCHMIDT et al., 2011). De natureza
								multifatorial, a obesidade é um dos elementos
								mais importantes para explicar o aumento da
								carga das doenças crônicas não transmissíveis
								(DCNT). O aumento contínuo dessa enfermidade em
								várias partes do mundo pode significar sérios
								problemas relativos aos custos sociais para
								indivíduos e serviços de saúde.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container className="espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<img
								src={img05}
								className="responsive w60 centerImg"
							/>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Os tratamentos oferecidos às pessoas obesas a
								fim de cuidar das consequências decorrentes da
								doença representam enormes gastos no setor da
								saúde. Os custos diretos contemplam serviços de
								prevenção, diagnóstico e tratamentos tanto para
								a própria obesidade quanto às comorbidades
								associadas. Além disso, necessitam adaptações na
								infraestrutura na própria comunidade dos locais
								públicos e/ou privados para pessoas obesas,
								como: camas reforçadas, mesas de operação e
								cadeiras de rodas; catracas ampliadas e assentos
								em estádios, modificações para o transporte,
								entre outras (WOF, 2015).{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								No Brasil, os custos de hospitalização relativos
								ao sobrepeso e à obesidade e a doenças
								associadas foram estimados por dados das
								hospitalizações de homens e mulheres de 20 a 60
								anos do Sistema de Informações Hospitalares do
								Sistema Único de Saúde (SIH-SUS), indicando um
								total equivalente a 3,02% para os homens e 5,83%
								para as mulheres, dos custos totais de
								hospitalização. Os resultados correspondem a 6,8
								e 9,3%, respectivamente, em relação aos demais
								motivos de hospitalização (excluindo as
								gestantes).{" "}
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Veja que a obesidade pode acarretar em hospitalização em decorrência de suas complicações e de sua causa multifatorial. Isso reforça a necessidade de articulação do setor saúde na realização de ações intersetoriais, de maneira a promover uma atenção integral. "
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Dessa maneira, o tratamento e o acompanhamento
								da obesidade e de doenças a ela associadas têm
								consequências econômicas relevantes para os
								serviços de saúde, configurando-se não somente
								com forte repercussão para a economia de um
								país.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								A compreensão da complexidade dessa doença é
								fundamental para o enfrentamento, uma vez que os
								esforços de construção da{" "}
								<ModalPopUp
									conceito={" intrassetorialidade "}
									textoConceito={
										"A intrassetorialidade corresponde a todos os níveis de atenção e esferas de gestão do SUS."
									}
								></ModalPopUp>{" "}
								e da{" "}
								<ModalPopUp
									conceito={" intersetorialidade "}
									textoConceito={
										"A intrassetorialidade corresponde a todos os níveis de atenção e esferas de gestão do SUS."
									}
								></ModalPopUp>{" "}
								podem contribuir para uma maior integração e
								efetividade do conjunto de medidas de prevenção
								e controle das DCNT. No âmbito do SUS, a
								proposta das redes de atenção à saúde, que
								inclui a linha de cuidado da obesidade,
								consolida-se em um contexto de valorização
								crescente das ações da atenção básica e de
								organização da rede de serviços (BRASIL, 2012).
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Entende-se que a obesidade envolve uma complexa
								relação entre corpo-saúde-alimento-sociedade e
								essas inter-relações influenciam nas condições
								para a promoção da saúde. Promover saúde é
								capacitar a sociedade para estar atenta à
								melhoria de sua qualidade de vida (AYRES, 2003),
								porém não é uma tarefa que se restringe à
								especificidade de um profissional, pois
								necessita de ações intersetoriais do poder
								público em parceria com os diversos setores da
								sociedade (BRASIL, 2006).
							</p>
						</Col>
					</Row>
				</Container>
				
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento espacamentoBottom degradeTitulo3"
					}
					titulo={"ENCERRAMENTO DA UNIDADE"}
				/>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Nesta unidade abordamos os aspectos
								multifatoriais que envolvem o sobrepeso e a
								obesidade, percebendo essas doenças como doença
								crônica não transmissível. O conhecimento sobre
								os determinantes sociais e o panorama
								epidemiológico é fundamental para identificação
								dos usuários com esses agravos de saúde e,
								principalmente, para o reconhecimento dessa
								problemática como uma questão relevante a ser
								abordada na Atenção Básica.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

const Caixa1 = () => (
	<Container className="espacamentoBottom">
		<Row>
			<Col md="12" lg="12">
				<CaixaTemplate
					textoClique={"clicando no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaSaibaMais"}
					icone={iconeSaibaMais}
					textoUnidade={
						"Confira também informações sobre o consumo alimentar de carnes com excesso de gordura, leite com teor integral de gordura, alimentos doces em cinco ou mais dias da semana, refrigerantes, feijão, substituição de almoço e jantar por lanches, prática de atividade física, "
					}
					link={
						"http://www.abeso.org.br/uploads/downloads/100/5949633674659.pdf"
					}
					inserirLink={true}
					textoLink={"acesse clicando aqui."}
					inserirLinkInicio={false}
					proporcaoCol1={1}
					proporcaoCol2={11}
				></CaixaTemplate>
			</Col>
		</Row>
	</Container>
);

export default Unidade;
