import React from "react";
import "font-awesome/css/font-awesome.min.css";
import "./css/Utilitarios.css";
import { Container, Row, Col } from "reactstrap";
import iconeLink from "../img/link.png";
import iconeVideo from "../img/video.png";
import ScrollAnimation from "react-animate-on-scroll";

export function espacamento(props) {
	return <div className="espacamento"></div>;
}

export function caixa(props) {
	return (
		<Container className={this.props.tipoCaixa}>
			<Row>
				<Col md="3" lg="3">
					<img src={this.props.icone} alt="" className="imgCaixa" />
				</Col>
				<Col md="9" lg="9">
					<p className="textoUnidade">{this.props.textoUnidade}</p>
				</Col>
			</Row>
		</Container>
	);
}

export const criarLink = (link, textoClique, icone, classImg) => {
	return (
		<a href={link} target="_blank">
			<b className="blackLink">
				{" " + textoClique + " "}
				<img src={icone} className={classImg} />
			</b>
		</a>
	);
};

export const criarIcone = (link, textoClique, icone, classImg) => {
	return (
		<a href={link} target="_blank">
			<b className="blackLink">
				{" " + textoClique + " "}
				<img src={icone} className={classImg} />
			</b>
		</a>
	);
};

export const criarIconeHide = (textoClique, icone, classImg) => {
	return (
		<b className="cursorIcone">
			{" " + textoClique + " "}
			<img src={icone} className={classImg} />
		</b>
	);
};

const TituloSecao = props => {
	return (
		<Container>
			<Row>
				<Col md="12" lg="12">
					<h1 className="tituloSecao">{props.titulo} </h1>
				</Col>
			</Row>
		</Container>
	);
};

const TituloSecao2 = props => {
	return (
		<Container>
			<Row>
				<Col md="12" lg="12">
					<h1 className="tituloSecao2">{props.titulo} </h1>
				</Col>
			</Row>
		</Container>
	);
};

const TituloSubSecao = props => {
	return (
		<Container>
			<Row>
				<Col md="12" lg="12">
					<p className="tituloSubSecao">{props.titulo} </p>
				</Col>
			</Row>
		</Container>
	);
};

const TituloSubSecao2 = props => {
	return (
		<Container>
			<Row>
				<Col md="12" lg="12">
					<p className="tituloSubSecao2">{props.titulo} </p>
				</Col>
			</Row>
		</Container>
	);
};

/*
const TituloSecaoBarra = (props) =>{
	return (
		<div className={props.tipoBarra + " degradeTitulo"}>
			<TituloSecao titulo={props.titulo} />
		</div>
	);
}*/
/*Alterar barras e deixar amarelo até o fim do titulo*/
const TituloSecaoBarra2 = props => {
	return (
		<div className={props.tipoBarra + " degradeTitulo2"}>
			<TituloSecao2 titulo={props.titulo} />
		</div>
	);
};

const TituloSecaoBarraNova = props => {
	return (
		<div className={props.tipoBarra + " marginBottom15"}>
			<TituloSecao2 titulo={props.titulo} />
		</div>
	);
};

const TituloSecaoBarraCustomizada = props => {
	return (
		<Row noGutters={true} className={props.tipoBarra + " "}>
			<Col md="6" lg="6" className="corTitulo">
				<h1 className="tituloSecaoCustom">{props.titulo} </h1>
			</Col>
			<Col md="6" lg="6" className="corTitulo2"></Col>
		</Row>
	);
};

const TituloSubSecaoBarra = props => {
	return (
		<div
			className={
				props.tipoBarra +
				" marginBottom15 barraSubTitulo degradeTitulo2"
			}
		>
			<TituloSubSecao2 titulo={props.titulo} />
		</div>
	);
};

const TituloSubSecaoBarraNova = props => {
	return (
		<div className={props.tipoBarra + " barraSubTitulo fundoIndex marginBottom15"}>
			<TituloSubSecao2 titulo={props.titulo} />
		</div>
	);
};

function CaixaLink(props) {
	return (
		<Container className={props.tipoCaixa + " borda10"}>
			<Row>
				<Col md="1" lg="1">
					<img
						src={props.icone}
						alt=""
						className="imgCaixa1 responsive"
					/>
				</Col>
				<Col md="11" lg="11">
					<p className="textoUnidade textoCaixa">
						<a href={props.link} target="_blank">
							Clique aqui
						</a>
						{" " + props.textoUnidade}
					</p>
				</Col>
			</Row>
		</Container>
	);
}

function CaixaLink12(props) {
	return (
		<Container className={props.tipoCaixa + " borda10"}>
			<Row>
				<Col md="1" lg="1">
					<img
						src={props.icone}
						alt=""
						className="imgCaixa1 responsive"
					/>
				</Col>
				<Col md="11" lg="11">
					<p className="textoUnidade textoCaixa">
						<a href={props.link}>
							{props.textoLink} target="_blank"{" "}
						</a>
						{" " + props.textoUnidade}
					</p>
				</Col>
			</Row>
		</Container>
	);
}

function IconeHide(props) {
	const { showing } = props.state;
	return (
		<b
			className="blackLink"
			onClick={() => this.setState({ showing: !showing })}
		>
			{" " + props.textoClique + " "}
			<img src={props.icone} className={props.classImg} />
		</b>
	);
}

function CaixaLink12Condicional(props) {
	const inserirLinkInicio = props.inserirLinkInicio;
	if (inserirLinkInicio) {
		return (
			<Container className={props.tipoCaixa + " borda10"}>
				<Row>
					<Col md="1" lg="1">
						<img src={props.icone} alt="" className="imgCaixa1" />
					</Col>
					<Col md="11" lg="11">
						<p className="textoUnidade textoCaixa">
							<a href={props.link}>
								{props.textoLink} target="_blank"{" "}
							</a>
							{" " + props.textoUnidade}
						</p>
					</Col>
				</Row>
			</Container>
		);
	}
	return (
		<Container className={props.tipoCaixa + " borda10"}>
			<Row>
				<Col md="1" lg="1">
					<img src={props.icone} alt="" className="imgCaixa1" />
				</Col>
				<Col md="11" lg="11">
					<p className="textoUnidade textoCaixa">
						{props.textoUnidade}
						<a href={props.link}>
							{props.textoLink} target="_blank"{" "}
						</a>
					</p>
				</Col>
			</Row>
		</Container>
	);
}

{
	/*Caixa em 12 a proporcao é 2 para 10*/
}
function CaixaGeral(props) {
	const inserirLinkInicio = props.inserirLinkInicio;
	var text = (
		<p className="textoUnidade textoCaixa">
			<a href={props.link}>{props.textoLink} target="_blank"</a>
			{" " + props.textoUnidade}
		</p>
	);
	if (!inserirLinkInicio) {
		text = (
			<p className="textoUnidade textoCaixa">
				{props.textoUnidade}
				<a href={props.link}>{props.textoLink} target="_blank"</a>
			</p>
		);
	}

	return (
		<Container className={props.tipoCaixa + " borda10"}>
			<Row>
				<Col
					md={props.proporcaoCol1}
					lg={props.proporcaoCol1}
					xs={props.proporcaoCol1}
					sm={props.proporcaoCol1}
				>
					<img src={props.icone} alt="" className="imgCaixa" />
				</Col>
				<Col
					md={props.proporcaoCol2}
					lg={props.proporcaoCol2}
					xs={props.proporcaoCol2}
					sm={props.proporcaoCol2}
				>
					{text}
				</Col>
			</Row>
		</Container>
	);
}

const CaixaGeralRetorno = props => {
	const inserirLinkInicio = props.inserirLinkInicio;
	var text = (
		<p className="textoCaixa">
			<a target="_blank" href={props.linkPt1}>
				{props.textoLinkPt1}
			</a>
			{" " + props.textoUnidadePt1}
			{" " + props.textoUnidadePt2}
			<a target="_blank" href={props.linkPt2}>
				{props.textoLinkPt2}{" "}
			</a>
		</p>
	);
	if (!inserirLinkInicio) {
		text = (
			<p className="textoUnidade textoCaixa">
				{props.textoUnidade}
				<a target="_blank" href={props.link}>
					{props.textoLink}{" "}
				</a>
			</p>
		);
	}

	return (
		<Container className={props.tipoCaixa + " borda10"}>
			<Row>
				<Col md={props.proporcaoCol1} lg={props.proporcaoCol1}>
					<img src={props.icone} alt="" className="imgCaixa1" />
				</Col>
				<Col md={props.proporcaoCol2} lg={props.proporcaoCol2}>
					{text}
				</Col>
			</Row>
		</Container>
	);
};

function Caixa6(props) {
	return (
		<Container className={props.tipoCaixa + " borda10"}>
			<Row>
				<Col md="1" lg="1">
					<img src={props.icone} alt="" className="imgCaixa1" />
				</Col>
				<Col md="11" lg="11">
					<p className="textoUnidade textoCaixa">
						{props.textoUnidade}
					</p>
				</Col>
			</Row>
		</Container>
	);
}

const CaixaTemplate = props => {
	const inserirLink = props.inserirLink;
	var text = <p className="textoCaixa">{props.textoUnidade}</p>;
	var paddingEsquerda = "";
	if (inserirLink) {
		const inserirLinkInicio = props.inserirLinkInicio;
		text = (
			<p className="textoCaixa">
				<a href={props.link} target="_blank">
					{props.textoLink}
				</a>
				{" " + props.textoUnidade}
			</p>
		);
		if (!inserirLinkInicio) {
			text = (
				<p className="textoCaixa">
					{props.textoUnidade}
					<a href={props.link}>{props.textoLink} </a>
				</p>
			);
		}
	}

	return (
		<Container className={props.tipoCaixa + " borda10"} id="caixa">
			<Row noGutters={true} className="marginLeftNegative15">
				<Col
					md={props.proporcaoCol1}
					lg={props.proporcaoCol1}
					xs={props.proporcaoCol1}
					sm={props.proporcaoCol1}
				>
					<ScrollAnimation
						animateIn={"flipInY"}
						duration={2}
						delay={2}
						animateOnce={true}
					>
						<img src={props.icone} alt="" className="imgCaixaPP" />
					</ScrollAnimation>
				</Col>
				<Col
					md={props.proporcaoCol2}
					lg={props.proporcaoCol2}
					xs={props.proporcaoCol2}
					sm={props.proporcaoCol2}
					className={paddingEsquerda}
				>
					{text}
				</Col>
			</Row>
		</Container>
	);
};

export {
	TituloSecaoBarra2,
	TituloSubSecaoBarra,
	CaixaTemplate,
	CaixaGeralRetorno,
	TituloSecaoBarraNova,
	TituloSubSecaoBarraNova,
	TituloSecaoBarraCustomizada
};
