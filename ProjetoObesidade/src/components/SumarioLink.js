import React from "react";
import "./css/Sumario.css";
import "font-awesome/css/font-awesome.min.css";
import { Container, Row, Col } from "reactstrap";
import "./css/Utilitarios.css";
import { HashLink as Link } from "react-router-hash-link";

class Sumario extends React.Component {
	render() {
		const titulosPt1 = this.props.titulosPt1;
		const listItems = titulosPt1.map(itens => (
			<li className="tituloUnidade">
				{
					<Link
						to={itens.id}
						smooth={true}
						scroll={el =>
							el.scrollIntoView({
								behavior: "smooth",
								block: "center",
								alignToTop: false
							})
						}
						className="linkSumario"
					>
						{itens.title}
					</Link>
				}
			</li>
		));

		const titulosPt2 = this.props.titulosPt2;
		const listItems2 = titulosPt2.map(itens => (
			<li className="tituloUnidade">
				{
					<Link
						to={itens.id}
						scroll={el =>
							el.scrollIntoView({
								behavior: "smooth",
								block: "center"
							})
						}
						className="linkSumario"
					>
						{itens.title}
					</Link>
				}
			</li>
		));

		return (
			<div
				className={
					this.props.tipoSumario + " espacamento logoCentralSumario"
				}
			>
				<Container className="">
					<Row>
						<Col md="12" lg="12">
							<p className="tituloSumario">Conteúdo</p>
						</Col>
						<Col md="12" lg="12">
							<ul className="centerBullets">{listItems}</ul>
							<ul className="centerBullets">{listItems2}</ul>
						</Col>

						<Col md="12" lg="12">
							<br />
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Sumario;
