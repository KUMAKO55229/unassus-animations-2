var $jq = jQuery.noConflict();

jQuery.fn.rotate = function(degrees) {
    $jq(this).css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
                 '-moz-transform' : 'rotate('+ degrees +'deg)',
                 '-ms-transform' : 'rotate('+ degrees +'deg)',
                 'transform' : 'rotate('+ degrees +'deg)'});
    return $jq(this);
};


jQuery( document ).ready(function( $jq ) {
// Handler for .ready() called.

/**
 * 
 */

 
var angle = 0;
var wheelAngle = angle;
var hoyAngle;
var angleDifference;
var dragging = false;
var dragTimeout;
var wheelPosition = $jq("#wheel").offset();
var wheelWidth = $jq("#wheel").width();
var wheelHeight = wheelWidth;
var wheelCentreX = Math.round(wheelPosition.left + (wheelWidth/2));
var wheelCentreY = Math.round(wheelPosition.top + (wheelHeight/2));
var updateDataTimeout;
var FURDate;
var dueDate;

var d = new Date();
var linuxzero = new Date(d.getFullYear(), 0, 1);
var theYear = d.getFullYear();
var theMonth = d.getMonth(); // Jan 0, Feb 1, March 3 ...
var theDay = d.getDate(); // 1-31
var isLeap = new Date(theYear,1,29).getDate() == 29;
var prev_isLeap = new Date(theYear-1,1,29).getDate() == 29; 
var numberOfDays = 365;
var prev_NumberOfDays = numberOfDays;
//check if Year is Leap
if(isLeap) {
	numberOfDays = 366;
}
else if(prev_isLeap) {// if it is not, check if prev Year was Leap
	prev_NumberOfDays = 366;
}

// calculate the current_Day of the year. Ex. 10th January is day 10 of the year
var current_DayOfTheYear = Math.round(((d - linuxzero) / 1000 / 60 / 60 / 24) + .5, 0);
// calculate hoy angle
hoyAngle = 360 * (current_DayOfTheYear/numberOfDays);

// set initial hoy angle
$jq("#hoy-image").rotate(hoyAngle);

// set initial wheel angle
$jq("#wheel-image").rotate(wheelAngle);

// Listeners
$jq("#wheel").mousedown(function(e){
	
	dragging = true;
	
	//calculate the difference between the cursorAngle and the current wheelAngle
	angle = calculateCursorAngle(e)
	angleDifference = angle - wheelAngle;
	
	$jq("#wheelContainer").mousemove(function(e){
		
		//if dragging
		if (dragging) {
			//alert("aha");
			angle = calculateCursorAngle(e);
			wheelAngle = angle - angleDifference;
			
			if(wheelAngle > 360)	wheelAngle -= 360;
			else if(wheelAngle < 0) wheelAngle = 360 + wheelAngle;

			// visual feedback - rotate wheel
			$jq("#wheel-image").rotate(wheelAngle);
			
			// update data
			updateData(wheelAngle);
		}
		
		return false
	});
	
	$jq("#wheelContainer").one('mouseup', function() {
	    //alert("This will show after mousemove and mouse released.");
		dragging = false;
		$jq("#wheelContainer").unbind();
		return false;
		
	});
	
	// Using return false prevents browser's default,
	// often unwanted mousemove actions (drag & drop)
	return false;

	
});

$jq("#wheel").mouseout(function(e){
	dragging = false;
	return false;
});		


function calculateCursorAngle(e) {
	var cursorAngle;
	var pageCoords = "( " + e.pageX + ", " + e.pageY + " )";
	var clientCoords = "( " + e.clientX + ", " + e.clientY + " )";
	
	var mouseX = e.pageX;
	var mouseY = e.pageY;
	
	//$jq("span:first").text("( e.pageX, e.pageY ) - " + pageCoords);
	//$jq("span:last").text("( e.clientX, e.clientY ) - " + clientCoords);
	
	// calculate the angle
	
	// 1. find info about the right triangle : wheelCentre, cursor, intersection of the deltaX and deltaY
	var deltaX = wheelCentreX - mouseX;
	var deltaY = wheelCentreY - mouseY;
	var hipotenusa = Math.round( Math.sqrt( (deltaX*deltaX) + (deltaY*deltaY) ) );
	// the angle value
	var radians = Math.asin(deltaX/hipotenusa);
	var angleBuffer = (radians*180)/Math.PI;
	//$jq("span:first").text("angle>>>:"+angleBuffer);
	
	
	if(mouseX >= wheelCentreX && mouseY < wheelCentreY){
		cursorAngle = - angleBuffer;
		//$jq("span:last").text("1: angle:"+angleBuffer);
	}
	else if(mouseX > wheelCentreX && mouseY >= wheelCentreY){
		cursorAngle = 180 + angleBuffer;
		//$jq("span:last").text("2: angle:"+angleBuffer);
	}
	else if(mouseX <= wheelCentreX && mouseY > wheelCentreY){
		cursorAngle = 180 + angleBuffer;
		//$jq("span:last").text("3: angle:"+angleBuffer);
	}
	else if(mouseX < wheelCentreX && mouseY <= wheelCentreY){
		cursorAngle = 360 - angleBuffer;
		//$jq("span:last").text("4: angle:"+angleBuffer);
	}
	//else {
	//	alert("calculateCursorAngle->Exception");
	//}
	
	// round angle value
	//cursorAngle = Math.round(cursorAngle);
	return cursorAngle;
	
}

function angleToDayOfTheYear(_angle){
	var dayOfTheYear = Math.round( _angle/(360/numberOfDays) );
	//$jq("span:first").text("day:"+day+" angle: "+wheelAngle);
	return dayOfTheYear;
}

function dateFromDay(year, day){
  var date = new Date(year, 0); // initialize a date in `year-01-01`
  return new Date(date.setDate(day)); // add the number of days
}

function furToDueDate(_FURDOY) {
	var dueDay = (_FURDOY + 280) < 365? _FURDOY + 280 : _FURDOY + 280 - 365;//267
	//$jq("div#ad-300x250").html(_FURDOY);
	var dueDate = dateFromDay(theYear,dueDay);
	return dueDate;
}

			
function updateData(_wheelAngle) {
	var FUR_dayOfTheYear = angleToDayOfTheYear(_wheelAngle);
	
	var dayOfPregnancy = (current_DayOfTheYear > FUR_dayOfTheYear)? current_DayOfTheYear-FUR_dayOfTheYear : current_DayOfTheYear + (prev_NumberOfDays - FUR_dayOfTheYear);
	var weekOfPregnancy = Math.ceil(dayOfPregnancy/7);
	FURDate = dateFromDay(theYear,FUR_dayOfTheYear);
	dueDate = furToDueDate(FUR_dayOfTheYear);
	//$jq("span:first").text("dayOfPregnancy:"+dayOfPregnancy+" weekOfPregnancy: "+weekOfPregnancy);
	//var week = 
	
	
	// Visual Feedback: update data : F.U.R.(last menstrual date) , Fecha de parto(Due Date)
	
	
	// visual feedback - Wheel Center
	$jq("div#center-lastMenstrual").html(spanishDate(FURDate));//spanishDate(dueDate)
	$jq("div#center-dueDate").html(spanishDate(dueDate));//spanishDate(dueDate)
	
	
	// get data from server
	clearTimeout(updateDataTimeout);
	updateDataTimeout = setTimeout(function() {
		fetchData(weekOfPregnancy);
	},500);
	
}

function fetchData(_week) {

	//use AJAX to fetch next pictures
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
		    //document.getElementById("result-fecha-actual").innerHTML=xmlhttp.responseText;
			eval(xmlhttp.responseText);
			$jq("#result-fecha-actual").html(spanishDate(d));
			$jq("#result-parto").html(spanishDate(dueDate));
			$jq("#result-fur").html(spanishDate(FURDate));
			$jq("#result-dbp").text(result_longitud);
			$jq("#result-longitud").text(result_longitud);
			$jq("#result-peso").text(result_peso);
			$jq("#result-talla").text(result_talla);
			var enlace = $jq("#enlace").html();
			//$jq("#").text();
			if(_week > 0 && _week <43){
				$jq("#result-semana").html("<a href='https://semanas.elembarazo.net/semana" + _week + "embarazo.html'>" + _week + "</a>");
				$jq("#imagen-semana").html("<img class='thumbnail' src='https://www.the-pregnancy.net/wp-content/uploads/2012/07/semana" + _week + "-150x150.jpg'>");
				$jq(".popup").attr("href", "https://www.facebook.com/sharer.php?s=100&p[url]=" + enlace + "&p[images][0]=https://www.the-pregnancy.net/wp-content/uploads/2012/07/semana" + _week + "-150x150.jpg&p[title]=Estoy embarazada de " + _week + " semanas&p[summary]=?He utilizado el Gestograma del embarazo y parece que estoy embarazada de " + _week + " semanas.");
			}else{
				$jq("#result-semana").html("NÃO está grávida");
				$jq("#result-semanas").html("<a href='https://semanas.elembarazo.net/'>semanas de embarazo</a>");
			}	
	    }
	  }
	xmlhttp.open("GET","https://es.calcuworld.com/wp-content/themes/twentytwelve-child/semanas-gestograma.php?semana="+_week,true);
	xmlhttp.send();
}	


function _spanishDate(date){
	var weekday=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
	var monthname=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
	return '<span class="date-es-weekday">'+weekday[date.getDay()]+'</span> <span class="date-es-day">'+date.getDate()+'</span> <span class="date-es-month">'+monthname[date.getMonth()]+'</span>';
}




});

