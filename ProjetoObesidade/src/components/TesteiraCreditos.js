import "./css/Testeira.css";
import "font-awesome/css/font-awesome.min.css";
import { Container, Row, Col } from "reactstrap";
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from "reactstrap";

import "./css/animate.css";
import ScrollAnimation from "react-animate-on-scroll";
import Sticky from "react-sticky-el";

var React = require("react");

const styleSticky = {
	opacity: "1",
	zIndex: "1"
};

const styleAnimation = {
	animationFillMode: "none"
};

class Testeira extends React.Component {
	render() {
		return (
			<Sticky stickyStyle={styleSticky}>
				<div className={this.props.tipoBarra}>
					<Container fluid={true}>
						<Row>
							<Col md="12" lg="12" xs="12" sm="12">
								<p className="tituloUnidadeTesteira">
									{this.props.nomeUnidadePt1}
									<br />
									{this.props.nomeUnidadePt2}
								</p>
								<UncontrolledDropdown className="alignRightIconCreditos">
									<DropdownToggle nav>
										<a>
											<img
												src={this.props.menuHamburguer}
												className="imgResponsive logoRight"
											/>
										</a>
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem>
											<a
												href="../guia.pdf"
												target="_blank"
												className="cursorHamburger"
											>
												Guia do curso
											</a>
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem>
											<a
												href="index.html#/creditos"
												target="_blank"
												className="cursorHamburger"
											>
												Créditos
											</a>
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem>
											<a
												href="../livro.pdf"
												target="_blank"
												className="cursorHamburger"
											>
												Livro do curso
											</a>
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</Col>
							
						</Row>
					</Container>
				</div>
			</Sticky>
		);
	}
}

export default Testeira;
