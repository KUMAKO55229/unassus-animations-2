import React, { Component } from "react";
import {
  Collapse,
  CardBody,
  Card,
  CardHeader,
  Container,
  Row,
  Col
} from "reactstrap";

const titulos = [];

const items = [
  {
    texto: (
      <div className="fundoSliderTexto">
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                Por volta de 70% das gestantes apresentam pelo menos um desses
                sintomas. Náuseas, vômitos e azia ocorrem geralmente nos
                primeiros meses de gravidez como resultado das alterações
                hormonais comuns na gestação. Tranquilize a gestante informando
                que esses sintomas são normais e que existem algumas estratégias
                para aliviá-los. Em folders e cartazes você pode referir a
                importância de:{" "}
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • fazer pequenas refeições em ambiente arejado, com intervalos
                de 2 horas;
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • restringir os alimentos com odores fortes e consumi-los em
                pequenas quantidades;{" "}
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • optar pelos cereais bem cozidos, os pães e torradas caseiras
                com geleia, as batatas bem cozidas, os ovos cozidos e a carne
                magra;{" "}
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • evitar os alimentos irritantes (exemplo: o café, o chá
                preto/verde, o chocolate e comida muito condimentada);{" "}
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • ingerir, de acordo com a tolerância individual, líquidos
                frios, cerca de 1 a 2 horas, antes e após as refeições.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    ),
    titulo: (     
      "Náuseas, vómitos e azia"
    )
  },
  {
    texto: (
      <div className="fundoSliderTexto">
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                Aproximadamente 35% a 40% das mulheres grávidas passam por
                crises de constipação na gestação. Para prevenir ou aliviar esse
                sintoma, a gestante pode:
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • beber bastantes líquidos, nomeadamente água (2 litros por
                dia);
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                • aumentar a ingestão de alimentos ricos em fibra (pão integral,
                arroz integral, cereais integrais, legumes e frutas frescas e
                secas, especialmente ameixas e figos, quando acessíveis).
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    ),
    titulo: (     
        "Constipação"
      
    )
  },
  {
    texto: (
      <div className="fundoSliderTexto">
        <Container>
          <Row>
            <Col md="12" lg="12">
              <p className="textoUnidade">
                Muitas mulheres referem sentir mais apetite que o normal depois
                que engravidam e sentem-se mal quando não ingerem alimentos ou
                bebidas em um curto espaço de tempo. Nessas situações, oriente a
                realização de pequenas refeições com intervalos menores de
                tempo.
              </p>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="6" lg="6">
              <p className="textoUnidade">
                Você pode sugerir que as gestantes façam o desjejum, dois
                pequenos lanches matinais (compostos por fruta/castanhas, em
                regiões de fácil acesso), almoço com fruta de sobremesa, dois
                pequenos lanches vespertinos, jantar e ceia. Nesses casos, as
                refeições principais devem ser completas e os lanchinhos
                servidos em pequenas porções. Esses aspectos podem ser
                realizados em formato de oficina, solicitando para as gestantes
                reproduzirem, em uma cartolina, os horários e alimentos que
                colocam no prato nas diversas refeições.
              </p>
            </Col>
            <Col md="6" lg="6">
              <p className="textoUnidade">
                {" "}
                Questione quantas delas sofrem de apetite em excesso e
                ansiedade. Depois, apresente, em slides ou em cartolina, um
                esquema do número de refeições recomendado e a importância de se
                alimentar em pequenas quantidades várias vezes ao dia. Também é
                importante que você lembre às gestantes sobre a importância do
                consumo adequado de água, já que, muitas vezes, situações de
                desidratação inicial podem ser confundidas com fome.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    ),
    titulo: (      
        "Apetite em excesso/ansiedade"
    )
  }
];
class Accordion extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      collapse: 1,
      cards: [1, 2, 3],
      itens: [items[0].titulo, items[1].titulo, items[2].titulo],
      textos: [items[0].texto, items[1].texto, items[2].texto]
    };
  }

  toggle(e) {
    let event = e.target.dataset.event;
    this.setState({
      collapse: this.state.collapse === Number(event) ? 0 : Number(event)
    });
  }
  render() {
    const { cards, collapse, itens, textos } = this.state;
    const slides = items.map(item => {
      return (
        <Container>
          <Row>
            <Col md="12" lg="12">
              {item.titulo}
            </Col>
          </Row>
        </Container>
      );
    });
    return (
      <div className="container">
        {cards.map(index => {
          return (
            <Card style={{ marginBottom: "1rem" }} key={index}>
              <CardHeader onClick={this.toggle} data-event={index}>
                <p onClick={this.toggle} data-event={index}><b>{itens[index - 1]}</b></p>
              </CardHeader>
              <Collapse isOpen={collapse === index}>
                <CardBody>{textos[index - 1]}</CardBody>
              </Collapse>
            </Card>
          );
        })}
      </div>
    );
  }
}

export default Accordion;
