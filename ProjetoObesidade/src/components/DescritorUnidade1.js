/* eslint-disable import/first */
import React from "react";
import Testeira from "./Testeira.js";
import CapaUnidade from "./capa/CapaPP.js";
import Sumario from "./SumarioLink.js";
import Unidade from "./Unidade1.js";
import menuHamburguer from "../img/menuObesidade2.png";
import ReturnStickyButton from "./ReturnStickyButton.js";

import Botoes from "./botoes/Botoes.js";


import un1 from "../img/un1/un1.png";
const testeira = {
  nomeCursoPt1: "Portfólio de animações",
  nomeCursoPt2: "",
  nomeUnidadePt1: "PORTFÓLIO DE ANIMAÇÕES",
  tipoBarra: "barraHamburguerLogo"
};

const sumario = {
  qntColunas: 2,
  tipoSumario: "sumarioUnidade"
};

const sumarioPt1 = [
  { id: "/un1#textoImgQuadro", title: "ANIMAÇÕES HTML" },
  { id: "/un1#sec1", title: "Animação 1 - Fade in" },
  { id: "/un1#sec2", title: "Animação 2 - Fade Out" },
  { id: "/un1#sec3", title: "Animação 3 - Sliders" },
];

const sumarioPt2 = [  
  { id: "/un1#sec4", title: "Animação 4 - Ícone Dropodown" },
  { id: "/un1#sec5", title: "Animação 5 - Quadro Dropodown" },
  { id: "/un1#sec6", title: "Animação 6 - Sequência de quadros Dropodown" },
  { id: "/un1#sec7", title: "Animação 7 - Quadro dropwdown dividido" },
  { id: "/un1#sec8", title: "Animação 8 - Fundo vazio" },
  { id: "/un1#sec9", title: "Animação 9 - Modal" },
  { id: "/un1#secINT", title: "Animação 10 - STORYLINE" },
  { id: "/un1#sec11", title: "Animação 11 - Vídeo na página" },
  { id: "/un1#sec12", title: "Animação 12 - Slide automático" },
  { id: "/un1#sec13", title: "Animação 13 - Menu Dropdown com pesquisa de itens" },
  { id: "/un1#sec14", title: "Animação 14 - PULSE" },
  { id: "/un1#sec15", title: "Animação 15 - HEADSHAKE" },
  { id: "/un1#sec16", title: "Animação 16 - BOUNCE IN" },
  { id: "/un1#sec17", title: "Animação 17 - BOUNCE IN LEFT" },
  { id: "/un1#sec18", title: "Animação 18 - ROTATE-IN UP-LEFT" },
  { id: "/un1#sec19", title: "Animação 19 - ZOOM-IN" },
  { id: "/un1#sec20", title: "Animação 20 - SLIDE-IN LEFT" },
  { id: "/un1#secEST", title: "ESTÁTICOS" },
  { id: "/un1#sec21", title: "Animação 21 - Imagem de Fundo" },
  { id: "/un1#sec22", title: "Animação 22 - Imagem de Fundo Lateral" },
  { id: "/un1#sec23", title: "Animação 23 - Fundo Lateral" },
  { id: "/un1#sec24", title: "Animação 24 - Figura de Respiro" },
  { id: "/un1#sec25", title: "Animação 25 - Imagem de fundo vazado" },
];

const linkCurso = "https://unasus-cp.moodle.ufsc.br/course/view.php?id=503";

const unidade = {
  tipoUnidade: "barraUnidade",
  corBarra: "yellow"
};

function DescritorUnidade1() {
  return (
    <div className="DescritorUnidade1">
      {/*<Testeira nomeCursoPt1={testeira.nomeCursoPt1} nomeCursoPt2 ={testeira.nomeCursoPt2} 
        nomeUnidadePt1={testeira.nomeUnidadePt1} nomeUnidadePt2={testeira.nomeUnidadePt2} 
        tipoBarra={testeira.tipoBarra} menuHamburguer={menuHamburguer}/> */}
      <div id="top">
        <CapaUnidade
          tituloPt1={testeira.nomeUnidadePt1}
          tituloPt2={testeira.nomeUnidadePt2}
        />
 
          <Sumario
            tipoSumario={sumario.tipoSumario}
            titulosPt1={sumarioPt1}
            titulosPt2={sumarioPt2}
        />
      </div>
    
      <Unidade tipoUnidade={unidade.tipoUnidade} corBarra={unidade.corBarra} />      
  
    </div>
  );
}

export default DescritorUnidade1;
