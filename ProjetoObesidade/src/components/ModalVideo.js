import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";

import iconeVideo from "../img/video.png";
import "./css/Utilitarios.css";
import "./css/Modal.css";

const styleModal = {};

class ModalVideo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    return (
      <b className="cursorIcone" onClick={this.toggle}>
        {" " + this.props.textoClique + " "}
        <img src={this.props.icone} className={this.props.classImg} />

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className="fundoModalVideo"
        >
          <Container className="corPopUp">
            <Row className="">
              <Col md="12" lg="12">
                <Button
                  onClick={this.toggle}
                  className="alinharDireitaIcone"
                  close
                ></Button>
              </Col>
              <Col md="12" lg="12">
                
                  <iframe
                    width="700"
                    height="400"
                    src={this.props.linkVideo}
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>
                
              </Col>
            </Row>
          </Container>
        </Modal>
      </b>
    );
  }
}

export default ModalVideo;
