import "font-awesome/css/font-awesome.min.css";
import "../css/animate.css";
import "./CapaUnidade.css";
import { Container, Row, Col } from "reactstrap";
import { Animated } from "react-animated-css";
import srcCapa from "./img/camadaN01.jpg";
import srcNome from "./img/camadaNome.png";

var React = require("react");

class CapaUnidade extends React.Component {
	render() {
		/*animar o background ao inves da imagem capa1*/
		return (
			<Animated
				animationIn="fadeIn"
				animationInDuration={2000}
				animationOutDuration={1000}
				isVisible={true}
				className=""
			>
				<div>
					<Container fluid={true}>
						<Row>
							<Col className="autoAdjustBackground2">
								<Animated
									animationIn="fadeIn"
									animationInDuration={3000}
									animationInDelay={2000}
									animationOutDuration={1000}
									isVisible={true}
								>	
								<div className="display">
									<p className="tituloCentralizado2">
										{this.props.tituloPt1}
										<br />
										{this.props.tituloPt2}
									</p>

								</div>
								</Animated>
							</Col>
						</Row>
					</Container>
				</div>
			</Animated>
		);
	}
}

export default CapaUnidade;
