import "font-awesome/css/font-awesome.min.css";
import "../css/animate.css";
import "./CapaUnidade.css";
import { Container, Row, Col } from "reactstrap";
import { Animated } from "react-animated-css";
import srcCapa from "./img/capaFundo-04.png";
import srcNome from "./img/camadaNome.png";
import logoCentral from "./img/logoCentral.png";
import arco from "./img/arco.png";

var React = require("react");

const styleLogo = {
	top: "50%",
	left: "50%",
	marginTop: "15%"

}


class CapaUnidade extends React.Component {
	render() {
		/*animar o background ao inves da imagem capa1*/
		return (
			<Animated
				animationIn="fadeIn"
				animationInDuration={2000}
				animationOutDuration={1000}
				isVisible={true}
				className=""
			>
				<div>
					<Container fluid={true}>
						<Row>
							<Col className="autoAdjustBackground2">
								<Animated
									animationIn="rotateIn"
									animationInDuration={3000}
									animationInDelay={2000}
									animationOutDuration={1000}
									style = {styleLogo}
								>
									<img
										src={logoCentral}
										className="logoCentral"
									/>
								</Animated>
								<Animated
									animationIn="fadeIn"
									animationInDuration={3000}
									animationInDelay={2000}
									animationOutDuration={1000}
									isVisible={true}
								>
									<div className="display">
										<p className="tituloCentralizado2">
											{this.props.tituloPt1}
											<br />
											{this.props.tituloPt2}
										</p>
									</div>
								</Animated>
								<Animated
									animationIn="fadeIn"
									animationInDuration={4000}
									animationInDelay={2000}
									animationOutDuration={1000}
									isVisible={true}
								>
									<img
										src={arco}
										className="arcoCentral"
									/>
								</Animated>
								<Animated
									animationIn="fadeIn"
									animationInDuration={4000}
									animationInDelay={2000}
									animationOutDuration={1000}
									isVisible={true}
								>
									<div className="display">
										<p className="autores">
											| Dalvan Antonio Campos |
											<br />
											| Francieli Cembranel |
											<br />
											| Ronaldo Zonta | 
											
										</p>
									</div>
								</Animated>
							</Col>
						</Row>
					</Container>
				</div>
			</Animated>
		);
	}
}

export default CapaUnidade;
