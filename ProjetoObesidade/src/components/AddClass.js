import React from "react";
import classnames from "classnames";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption,

	TabContent,
	TabPane,
	Nav,
	NavItem,
	NavLink,
	Card,
	CardTitle,
	CardText
} from "reactstrap";

import Observer from 'react-intersection-observer';
import TrackVisibility from 'react-on-screen';

import img01 from "../img/un1/img01.jpeg";

const ComponentToTrack = ({ isVisible }) => {
    const style = {
        display: isVisible ? 'block' : 'inline'
    };
    return (
	    <Container>
			<Row>
				<Col md="12" lg="12">
					<div  style={style}>
						<img src={img01} className="responsive w30"/>			
					</div>
				</Col>
			</Row>
		</Container>
	)
}

class AddClass extends React.Component {
	constructor(props) {
		super();
		this.state = {
			class: "premounted"
		};
	}



	render() {
		return(
			<div>
				<TrackVisibility>
		            <ComponentToTrack />
		        </TrackVisibility>
			</div>
		);
	}
	changeClass() {
		


	}
}

export default AddClass;