import React from "react";
import "./Botoes.css";
import "font-awesome/css/font-awesome.min.css";
import { Container, Row, Col } from "reactstrap";
import "../css/Utilitarios.css";
import { HashLink as Link } from "react-router-hash-link";

import arco from "../../img/arco.png";

class Botoes extends React.Component {
	render() {
		const un1Path = this.props.un1Path;
		const un2Path = this.props.un2Path;
		const un3Path = this.props.un3Path;
		const un1 = this.props.un1;
		const un2 = this.props.un2;
		const un3 = this.props.un3;

		return (
			<div className="espacamentoBottom">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<Container>
								<Row>
									<Col
										md={{ size: "4", offset: "4" }}
										lg={{ size: "4", offset: "4" }}
									>
										<img
											src={arco}
											className="responsive w100 arcoBotao"
										/>
									</Col>
									<Col
										md={{ size: "4", offset: "4" }}
										lg={{ size: "4", offset: "4" }}
									>
										<Container className="marginLeftBotao">
											<Row>
												<Col md="4" lg="4">
													<Link
														to={un1Path}
														smooth={true}
														scroll={el =>
															el.scrollIntoView({
																behavior:
																	"smooth",
																block: "center",
																alignToTop: false
															})
														}
														title="Clique para ir para unidade 1"
														className=""
													>
														<img
															src={un1}
															className="responsive linkBotao"
														/>
													</Link>
												</Col>
												<Col md="4" lg="4">
													<Link
														to={un2Path}
														smooth={true}
														scroll={el =>
															el.scrollIntoView({
																behavior:
																	"smooth"
															})
														}
														title="Clique para ir para unidade 2"
														className=""
													>
														<img
															src={un2}
															className="responsive linkBotao"
														/>
													</Link>
												</Col>
												<Col md="4" lg="4">
													<Link
														to={un3Path}
														smooth={true}
														scroll={el =>
															el.scrollIntoView({
																behavior:
																	"smooth"
																
															})
														}
														title="Clique para ir para unidade 3"
														className=""
													>
														<img
															src={un3}
															className="responsive linkBotao"
														/>
													</Link>
												</Col>
											</Row>
										</Container>
									</Col>
								</Row>
							</Container>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Botoes;