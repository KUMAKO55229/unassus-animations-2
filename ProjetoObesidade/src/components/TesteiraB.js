import './css/Testeira.css';
import 'font-awesome/css/font-awesome.min.css';
import { Container, Row, Col } from 'reactstrap';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';


var React = require('react');







class Testeira extends React.Component {

	render() {

		return (
			<div className={this.props.tipoBarra}>
				<Container fluid={true}> 
					<Row >
						<Col md="5" lg="5">
							
						</Col>

						<Col md="5" lg="5">						
							<p className = "tituloUnidade">{this.props.nomeUnidadePt1}<br/>{this.props.nomeUnidadePt2}</p>
						</Col>
						<Col md="2" lg="2">
							<UncontrolledDropdown>
				                <DropdownToggle nav>                                    
                                    <span><img src={this.props.menuHamburguer} className="imgResponsive img35 logoRight"/></span>

				                </DropdownToggle>
				                <DropdownMenu right>
				                  <DropdownItem>
				                    Livro 2
				                  </DropdownItem>
				                  <DropdownItem divider />
				                  <DropdownItem>
				                    Livro 3
				                  </DropdownItem>				                  
				                  <DropdownItem divider />
				                  <DropdownItem>
				                    Livro 4
				                  </DropdownItem>
				                </DropdownMenu>
			              	</UncontrolledDropdown>

						</Col>
						

					</Row>

				</Container>
			</div>
			);
	}
};

export default Testeira;
