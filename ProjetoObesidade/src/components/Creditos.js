import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "./css/Unidade3.css";
import "font-awesome/css/font-awesome.min.css";
import { Container, Row, Col } from "reactstrap";

import classnames from "classnames";

import clique from "../img/clique.png";

import { caixa } from "./Utilitarios.js";
import { criarLink } from "./Utilitarios.js";
import {
	TituloSecaoBarra2,
	TituloSubSecaoBarra,
	CaixaTemplate,
	CaixaGeralRetorno,
	TituloSecaoBarraNova,
	TituloSubSecaoBarraNova
} from "./Utilitarios.js";

import Modal from "./Modal.js";

class Creditos extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Container>
					<Row>
						<Col md="6" lg="6" className="espacamento">
							<p className="textoUnidade">
								<p>
									<b>GOVERNO FEDERAL</b>
									<br />
									<b>MINISTÉRIO DA SAÚDE</b>
									<br />
									Secretaria de Atenção Primária à Saúde
									(SAPS)
									<br />
									Departamento de Promoção da Saúde
									Estratégicas (DEPROS)
									<br />
									Coordenação-Geral de Alimentação e Nutrição
									<br />
								</p>
								<p>
									<b>
										UNIVERSIDADE FEDERAL DE SANTA CATARINA
									</b>
									<br />
									Reitor: Ubaldo Cesar Balthazar
									<br />
									Vice-Reitora: Alacoque Lorenzini Erdmann
									<br />
									Pró-Reitor de Pós-Graduação: Cristiane
									Derani
									<br />
									Pró-Reitor de Pesquisa: Sebastião Roberto
									Soares
									<br />
									Pró-Reitor de Extensão: Rogério Cid Bastos
								</p>
								<p>
									<b>
										CENTRO DE CIÊNCIAS DA SAÚDE <br />
									</b>
									Diretor: Celso Spada <br />
									Vice-Diretor: Fabrício de Souza Neves
								</p>
								<p>
									<b>
										DEPARTAMENTO DE SAÚDE PÚBLICA <br />
									</b>
									Chefe do Departamento: Fabrício Augusto
									Menegon <br />
									Subchefe do Departamento: Lúcio José Botelho
								</p>
								<p>
									<b>EQUIPE TÉCNICA DO MINISTÉRIO DA SAÚDE</b>
									<br />
									<b>
										Coordenação Nacional de Alimentação e
										Nutrição
									</b>
									<br />
									Gestora geral do Projeto: Sheila Rubia
									Lindner
									<br />
									Coordenação de Produção de Material: Elza
									Berger Salema Coelho
									<br />
								</p>
								<p>
									<b>Autoria do Material</b>
									<br />
									Silvia Ozcariz
									<br />
									Caroline Bandeira
									<br />
									Roxana Knobel
									<br />									
								</p>
							</p>
						</Col>
						<Col md="6" lg="6" className="espacamento">
							<p>
								<b>Equipe Editorial</b>
								<br />
								Carolina Carvalho Bolsoni
								<br />
								Thays Berger Conceição
								<br />
								Deise Warmling
								<br />
								Dalvan Antonio de Campos
								<br />
							</p>
							<p>
								<b> Assessoria Pedagógica</b>
								<br />
								Márcia Regina Luz
								<br />
							</p>
							<p>
								<b>Equipe Executiva</b>
								<br />
								Patrícia Castro
								<br />
								Gabriel Donadio Costa
								<br />
							</p>
							<p>
								<b>Identidade Visual e Projeto gráfico</b>
								<br />
								Pedro Paulo Delpino
								<br />
							</p>
							<p>
								<b>Design Instrucional</b>
								<br />
								Eduard Marquardt
								<br />
							</p>
							<p>
								<b>Diagramação/Finalização</b>
								<br />
								Laura Martins Rodrigues
								<br />
							</p>
							<p>
								<b>Esquemáticos/Infográficos</b>
								<br />
								Naiane Cristina Salvi
							</p>
							<p>
								<b>Desenvolvedor web</b>
								<br />
								Rodrigo Rodrigues Pires de Mello
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Creditos;
