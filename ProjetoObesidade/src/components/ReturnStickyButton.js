import "./css/ReturnStickyButton.css";
import "font-awesome/css/font-awesome.min.css";
import { Container, Row, Col } from "reactstrap";
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from "reactstrap";


import "./css/animate.css";
import ScrollAnimation from "react-animate-on-scroll";
import Sticky from "react-sticky-el";

import returnButton from "../img/returnButton.png";

var React = require("react");



const styleAnimation = {
	animationFillMode: "none"
};

class ReturnStickyButton extends React.Component {
	render() {
		return (		
			<Sticky stickyClassName={"returnButtonSticky"}>
				<a
					href="https://unasus-quali.moodle.ufsc.br/course/view.php?id=48"
					target="_blank"
					alt="clique aqui para retornar"
					title="clique aqui para retornar"
				>
					<img src={returnButton} className="returnButton" />
				</a>
			</Sticky>

		);
	}
}

export default ReturnStickyButton;
