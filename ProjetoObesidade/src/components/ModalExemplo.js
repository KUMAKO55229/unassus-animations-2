import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";

import imgExemplo from "../img/un2/exemplo.png";

import iconeVideo from "../img/video.png";
import "./css/Utilitarios.css";
import "./css/Modal.css";
import cliqueAqui from "../img/clique.png";

const styleModal = {};

class ModalPopUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    return (
      
        <span className="cursorIcone_2" onClick={this.toggle}>
          <Container className="">
            <Row>
              <Col md="10" lg="10" className="espacamento">
                <p>Clique aqui para conhecer um exemplo</p>
              </Col>
              <Col md="2" lg="2" xs="2" sm="2">
                <img src={cliqueAqui} className="responsive w40 imgTop" />
              </Col>
            </Row>
          </Container>

          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className="fundoPopUp2"
          >
            <Container className="corPopUp">
              <Row className="">
                <Col md="12" lg="12">
                  <Button
                    onClick={this.toggle}
                    className="alinharDireitaIcone"
                    close
                  ></Button>
                </Col>
                <Col md="12" lg="12">
                  <ModalBody>
                    <img src={imgExemplo} className="responsive w100 borda10" />
                  </ModalBody>
                </Col>
              </Row>
            </Container>
          </Modal>
        </span>
      
    );
  }
}

export default ModalPopUp;
