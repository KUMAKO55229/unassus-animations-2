import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";

import iconeVideo from "../img/video.png";
import "./css/Utilitarios.css";
import "./css/Modal.css";

const styleModal = {};

class ModalPopUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    return (
      <span className="cursorIcone_2" onClick={this.toggle}>
        {this.props.conceito}

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className="fundoPopUp"
        >
          <Container className="corPopUp">
            <Row className="">
              <Col md="12" lg="12">
                <Button
                  
                  onClick={this.toggle}
                  className="alinharDireitaIcone"
                  close
                >
                  
                </Button>
              </Col>
              <Col md="12" lg="12">
              <ModalBody>
                <p>{this.props.textoConceito}</p>
                <p>{this.props.textoConceito2}</p>
                </ModalBody>
              </Col>
            </Row>
          </Container>
        </Modal>
      </span>
    );
  }
}

export default ModalPopUp;
