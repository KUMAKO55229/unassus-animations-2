$(".btnFinalL").click(function(e) {
    e.preventDefault();
    window.location.href = url_voltar_ao_desafio;
});
$(".btnFinalR").click(function(e) {
    e.preventDefault();
    window.location.href = url_proxima_unidade;
});
$(".btn_black").hover(
    function() {
        $(this).css({
            "background-image": "url('img/btn_round_white.png')"
        });
    },
    function() {
        $(this).css({
            "background-image": "url('img/btn_round_black.png')"
        });
    }
);

$(".btn_white").hover(
    function() {
        $(this).css({
            "background-image": "url('img/btn_round_white.png')"
        });
    },
    function() {
        $(this).css({
            "background-image": "url('img/btn_round_black.png')"
        });
    }
);


function rollingFor(elemento) {
    $('html, body').animate({ scrollTop: $(elemento).offset().top - 60 }, 1000);
}



function repiasAnimation(isChrome, element, x, y, delay) {
    if (isChrome) {
        element.animate({ 'opacity': 1, 'background-position-x': +'' + x, 'background-position-y': +'' + y }, delay);
    } else {
        element.css({ 'opacity': 1, 'background-position': x + " " + y, '-moz-transition': 'all ' + delay + 'ms ease' });
    }
}

function repiasJoinAnimation(isChrome, leftElement, rightElement, delay) {
    if (isChrome) {
        leftElement.animate({ 'opacity': 1, 'background-position-x': '100%' }, delay);
        rightElement.animate({ 'opacity': 1, 'background-position-x': '0%' }, delay);
    } else {
        leftElement.css({ 'opacity': 1, 'background-position': '100% 0%', '-moz-transition': 'all ' + delay + 'ms ease' });
        rightElement.css({ 'opacity': 1, 'background-position': '0% 0%', '-moz-transition': 'all ' + delay + 'ms ease' });
    }

}

function repiasGetX(element) {
    var x = element.css('backgroundPosition').split(" ")[0];
    return x;
}

function repiasGetY(element) {
    var y = element.css('backgroundPosition').split(" ")[1];
    return y;
}

// MENU
$("#menu-close").click(function(e) {
    $("#sidebar-wrapper").toggleClass("active");
	return false;
});
 $("#menu-toggle").click(function(e) {
       $("#sidebar-wrapper").toggleClass("active");
	   return false;	
 });
$(document).click(function(e){
    if($("#sidebar-wrapper").hasClass('active')){
        $("#sidebar-wrapper").removeClass("active");
    }
});



// Scrolls to the selected menu item on the page
$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});


//ANIMATE:
$(document).ready(function() {
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	
    /* Every time the window is scrolled ... */
    $(window).scroll(function() {

        /* Check the location of each desired element */
        $('.paralaxable').each(function(i) {
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            if (bottom_of_window > bottom_of_object) {

                var secondClass = $(this).attr("class").split(' ')[1];

                var swz = $(window).width(); //screen width size

                switch (secondClass) {

                    case "fadein":
                        $(this).animate({ 'opacity': '1' }, 500);
                        break;



					case "capaContent":						
							repiasAnimation(isChrome, $(this), "50%", "30%", 1000);						
                        break;
						
                    case "movable":
                        repiasAnimation(isChrome, $(this), "0px", "0px", 1000);
                        break;

                    case "fadezoom":
                        if (swz <= 320) {
                            $(this).animate({ "fontSize": "1.7em", 'opacity': '1' }, 1000);
                            $(this).css("margin-top", "15px");
                        } else if (swz > 320 && swz <= 480) {
                            $(this).animate({ "fontSize": "1.8em", 'opacity': '1' }, 1000);
                            $(this).css("margin-top", "15px");
                        } else if (swz > 480 && swz <= 640) {
                            $(this).animate({ "fontSize": "2.3em", 'opacity': '1' }, 1000);
                            $(this).css("margin-top", "10px");
                        } else if (swz > 640 && swz <= 768) {
                            $(this).animate({ "fontSize": "2.3em", 'opacity': '1' }, 1000);
                            $(this).css("margin-top", "19px");
                        } else {
                            $(this).animate({ "fontSize": "2.4em", 'opacity': '1' }, 1000);
                            $(this).css("margin-top", "19px");
                        }

                        break;

                    /* novo */
					case "changeLogoLeftForLogo":
						$(".logoLeft").css({"background":"url(img/id/logo_interagindo.png) no-repeat", "margin-top":"0px"});
						break;

					/* novo */
					case "changeLogoLeftForUni":
						$(".logoLeft").css({"background":"url(img/id/uni1texto.png) no-repeat", "margin-top":"-13px"});
						$(".logoLeft").css("background-position", "50% 32px");				
						break;

                    case "changeTitleForTitle":
                        $(".title").html("Política Nacional de Atenção Integral à Saúde do Homem");
                        break;

                    case "changeTitleForTitleUni":
                        $(".title").html("Condições de vulnerabilidade na saúde do homem");
                        break;

                    case "changeTitleForTitleNo":
                        $(".title").html("");
                        break;

                    case "yellowBox":
                        repiasJoinAnimation(isChrome, $(".yellowBox1"), $(".yellowBox2"), 1000);
                        break;

                    case "blueBox":
                        repiasJoinAnimation(isChrome, $(".blueBox1"), $(".blueBox2"), 1000);
                        break;

                    case "yellowBorder":
                        repiasAnimation(isChrome, $(this), "50%", "0%", 1000);
                        break;

                    case "photoTextBox":
                        repiasJoinAnimation(isChrome, $(".photoTextBox1"), $(".photoTextBox2"), 1000);
                        break;

                    case "bubbles":
                        repiasAnimation(isChrome, $(this), $(this).css('backgroundPosition').split(" ")[0], "2px", 2500);
                        break;

                    case "animaOtherDrugs":
                        repiasAnimation(isChrome, $(".otherDrugs"), "100%", "50%", 1000);
                        break;

                    case "figura3":
                        repiasAnimation(isChrome, $(".figura32"), "0", "0", 1000);
                        repiasAnimation(isChrome, $(".figura33"), "0", "0", 1200);
                        repiasAnimation(isChrome, $(".figura34"), "50%", "0px", 1600);
                        repiasAnimation(isChrome, $(".figura35"), "50%", "0px", 1800);
                        break;


                    case "figura5":						
							repiasAnimation(isChrome, $(".figura53"), "100%", "0", 1200);
							repiasAnimation(isChrome, $(".figura54"), "0%", "0", 1200);
						
                        break;
					
					
						
						

                    case "pessoal":
                        repiasAnimation(isChrome, $(".pessoal"), "0px", "0px");
                        break;

                    case "figura6":					
						if (swz >= 769) {
                            repiasAnimation(isChrome, $(".figura61"), "0px", "0px", 800);
                        }                        
                        break;

                    case "figura7":
                        repiasAnimation(isChrome, $(".figura71"), "0px", "0px", 1200);
                        break;



                    case "graphic":
                        repiasAnimation(isChrome, $(".graphic_bar1"), "100%", "20px", 2000);
                        repiasAnimation(isChrome, $(".graphic_bar1_text"), repiasGetX($(".graphic_bar1_text")), repiasGetY($(".graphic_bar1_text")), 3000);

                        repiasAnimation(isChrome, $(".graphic_bar2"), "100%", "130px", 2000);
                        repiasAnimation(isChrome, $(".graphic_bar2_text"), repiasGetX($(".graphic_bar2_text")), repiasGetY($(".graphic_bar2_text")), 3000);

                        repiasAnimation(isChrome, $(".graphic_bar3"), "100%", "265px", 2000);
                        repiasAnimation(isChrome, $(".graphic_bar3_text"), repiasGetX($(".graphic_bar3_text")), repiasGetY($(".graphic_bar2_text")), 3000);

                        break;

                    case "session8":
                        if ($(window).width() <= 480) {
                            $(".diagonalText").animate({ "fontSize": "1em", 'opacity': '1' }, 1200);

                        } else {
                            $(".diagonalText").animate({ "fontSize": "1em", 'opacity': '1' }, 1200);
                        }
                        break;

                    case "apple":
                        repiasAnimation(isChrome, $(".apple1"), "0px", "0px", 1000);
                        repiasAnimation(isChrome, $(".apple2"), "0px", "0px", 1200);
                        repiasAnimation(isChrome, $(".apple3"), "0px", "0px", 1000);
                        break;

                    case "chart":
                        repiasAnimation(isChrome, $(".chart1"), "0px", "0px", 1000);
                        repiasAnimation(isChrome, $(".chart2"), "0px", "0px", 1500);
                        repiasAnimation(isChrome, $(".chart3"), "0px", "0px", 2000);
                        break;

                    case "animahas":
                        repiasAnimation(isChrome, $(".has"), "40%", "100%", 2500);
                        break;

                    case "bgCapa":
                        repiasAnimation(isChrome, $(".session3"), "0px", "0px", 1000);
                        break;
                }
            }
        });
    });
});

// ATIVIDADE
$(function() {
    var html1 = 'background:url(img/uni1/atividade/2.9.png) no-repeat; width:286px; height:532px; background-position: 0 -5px';
    var html2 = 'background:url(img/uni1/atividade/2.10.png) no-repeat; width:286px; height:532px; background-position: 0 -5px';
    var html3 = 'background:url(img/uni1/atividade/2.11.png) no-repeat; width:286px; height:532px; background-position: 0 -5px';

    function loadFeedback(n, html) {
        $(".atividade-q" + n + "-bg").attr("style",html);
		$(".atividade-q" + n + "-bg .atividade-alternativa1").hide();
		$(".atividade-q" + n + "-bg .atividade-alternativa2").hide();
		$(".atividade-q" + n + "-bg .atividade-alternativa3").hide();
    }

    $(".atividade-q1-bg .atividade-alternativa1 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(1, html1);
    });
    $(".atividade-q1-bg .atividade-alternativa2 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(1, html1);
    });
    $(".atividade-q1-bg .atividade-alternativa3 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(1, html1);
    });

    $(".atividade-q2-bg .atividade-alternativa1 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(2, html2);
    });
    $(".atividade-q2-bg .atividade-alternativa2 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(2, html2);
    });
    $(".atividade-q2-bg .atividade-alternativa3 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(2, html2);
    });

    $(".atividade-q3-bg .atividade-alternativa1 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(3, html3);
    });
    $(".atividade-q3-bg .atividade-alternativa2 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(3, html3);
    });
    $(".atividade-q3-bg .atividade-alternativa3 .atividade-btn").click(function(e) {
        e.preventDefault();
        loadFeedback(3, html3);
    });
});

$(function() {
    //vídeo 1
	var idVideo = "video1";
	var videoSrc = $("#"+idVideo+" iframe").attr("src");
	$("#"+idVideo).html('');
	
	$("#"+idVideo).click(function(e) {
        e.preventDefault();
		$(".media").html('<iframe src="'+videoSrc+'?autoplay=1" allowfullscreen="" frameborder="0"></iframe>');
	});
});
